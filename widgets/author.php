<?php
/**
 * Author Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

class Author extends Widget {
	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Author', 'vnh' ),
				'classname'   => 'widget-author',
				'description' => esc_html__( 'Display info of an author.', 'vnh' ),
				'fields'      => [
					'title'       => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Author Info', 'vnh' ),
					],
					'user_id'     => [
						'title'   => __( 'Choose an User:', 'vnh' ),
						'type'    => 'dropdown_all_users',
						'default' => 1,
					],
					'avatar_size' => [
						'title'   => __( 'Avatar Size:', 'vnh' ),
						'type'    => 'number',
						'options' => [
							'min' => 100,
							'max' => 200,
						],
						'default' => 200,
					],
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		$this->author_info( $instance );

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance                = $old_instance;
		$instance['title']       = sanitize_text_field( $new_instance['title'] );
		$instance['user_id']     = $new_instance['user_id'];
		$instance['avatar_size'] = ( (int) $new_instance['avatar_size'] !== 0 ) ? (int) $new_instance['avatar_size'] : null;

		return $instance;
	}

	public function author_info( $instance ) {
		$user_id = is_single() ? false : $instance['user_id'];
		$name    = get_the_author_meta( 'display_name', $user_id );
		$desc    = get_the_author_meta( 'user_description', $user_id );
		$avatar  = get_avatar( get_the_author_meta( 'ID', $user_id ), $instance['avatar_size'], '', '', [ 'class' => 'lazy' ] );
		$html    = '';

		$html .= sprintf( '<div class="widget-author__avatar">%s</div>', $avatar );
		$html .= sprintf( '<h3 class="widget-author__title">%s</h3>', esc_html( $name ) );
		$html .= $desc ? sprintf( '<div class="widget-author__content">%s</div>', wp_kses_post( $desc ) ) : null;

		echo $html; // WPCS XSS ok
	}
}
