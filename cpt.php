<?php

namespace VNH\Framework;

class Cpt {
	public $cpt_register;

	public function __construct() {
		$this->cpt_register = apply_filters( 'vnh/f/cpt/register', Base::$cpt_register );

		add_action( 'init', [ $this, 'register_post_types' ], 0 );
	}

	public function register_post_types() {
		foreach ( $this->cpt_register as $post_type => $args ) {
			$this->register_post_type( $post_type, $args );
		}
	}

	protected function register_post_type( $post_type, $args = [] ) {
		$default = [
			'labels'              => [
				'name_admin_bar'        => esc_html__( 'Post Type', 'vnh' ),
				'archives'              => esc_html__( 'Item Archives', 'vnh' ),
				'attributes'            => esc_html__( 'Item Attributes', 'vnh' ),
				'parent_item_colon'     => esc_html__( 'Parent Item:', 'vnh' ),
				'all_items'             => esc_html__( 'All Items', 'vnh' ),
				'add_new_item'          => esc_html__( 'Add New Item', 'vnh' ),
				'add_new'               => esc_html__( 'Add New', 'vnh' ),
				'new_item'              => esc_html__( 'New Item', 'vnh' ),
				'edit_item'             => esc_html__( 'Edit Item', 'vnh' ),
				'update_item'           => esc_html__( 'Update Item', 'vnh' ),
				'view_item'             => esc_html__( 'View Item', 'vnh' ),
				'view_items'            => esc_html__( 'View Items', 'vnh' ),
				'search_items'          => esc_html__( 'Search Item', 'vnh' ),
				'not_found'             => esc_html__( 'Not found', 'vnh' ),
				'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'vnh' ),
				'featured_image'        => esc_html__( 'Featured Image', 'vnh' ),
				'set_featured_image'    => esc_html__( 'Set featured image', 'vnh' ),
				'remove_featured_image' => esc_html__( 'Remove featured image', 'vnh' ),
				'use_featured_image'    => esc_html__( 'Use as featured image', 'vnh' ),
				'insert_into_item'      => esc_html__( 'Insert into item', 'vnh' ),
				'uploaded_to_this_item' => esc_html__( 'Uploaded to this item', 'vnh' ),
				'items_list'            => esc_html__( 'Items list', 'vnh' ),
				'items_list_navigation' => esc_html__( 'Items list navigation', 'vnh' ),
				'filter_items_list'     => esc_html__( 'Filter items list', 'vnh' ),
			],
			'supports'            => [ 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments' ],
			'taxonomies'          => [],
			'menu_icon'           => 'dashicons-portfolio',
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 20,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
			'show_in_rest'        => false,
			'rewrite'             => [
				'with_front' => true,
				'pages'      => true,
				'feeds'      => true,
			],
		];

		$args            = wp_parse_args( (array) $args, $default );
		$args['labels']  = wp_parse_args( (array) $args['labels'], $default['labels'] );
		$args['rewrite'] = wp_parse_args( (array) $args['rewrite'], $default['rewrite'] );

		register_post_type( $post_type, $args );
	}
}
