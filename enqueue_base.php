<?php

namespace VNH\Framework;

abstract class Enqueue_Base {
	public $enqueue;

	public function __construct() {
		$this->enqueue = apply_filters( 'vnh/f/enqueue/register', $this->enqueue );
		$this->setup_cache();

		add_action( 'wp_enqueue_scripts', [ $this, 'register_styles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles_and_scripts' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_comment_reply_js' ] );
	}

	abstract public function enqueue_styles_and_scripts();

	public function setup_cache() {
		$cached = is_dev() ? null : get_transient( Base::$transient_name['enqueue'] );
		if ( ! empty( $cached ) ) {
			$this->enqueue = $cached;
		}
		set_transient( Base::$transient_name['enqueue'], $this->enqueue );

		return $this->enqueue;
	}

	public function enqueue_comment_reply_js() {
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	public function register_styles() {
		foreach ( $this->enqueue['styles'] as $handle => $args ) {
			$this->register_style( $handle, $args );
		}
	}

	public function register_scripts() {
		foreach ( $this->enqueue['scripts'] as $handle => $args ) {
			$this->register_script( $handle, $args );
		}
	}

	private function register_script( $handle, $args ) {
		$defaults = [
			'deps'      => [],
			'version'   => null,
			'in_footer' => true,
			'have_min'  => false,
		];
		$args     = wp_parse_args( (array) $args, $defaults );
		$version  = Helper::flatten_version( $args['version'] );

		if ( is_dev() ) {
			wp_register_script( $handle, esc_url( $args['src'] ), $args['deps'], $version, $args['in_footer'] );
		}

		$src = $args['have_min'] === true ? str_replace( '.js', '.min.js', $args['src'] ) : $args['src'];

		wp_register_script( $handle, esc_url( $src ), $args['deps'], $version, $args['in_footer'] );
	}

	private function register_style( $handle, $args ) {
		$defaults = [
			'deps'    => false,
			'version' => null,
			'media'   => 'all',
			'has_rtl' => false,
		];
		$args     = wp_parse_args( (array) $args, $defaults );
		$version  = Helper::flatten_version( $args['version'] );

		wp_register_style( $handle, esc_url( $args['src'] ), $args['deps'], $version, $args['media'] );

		if ( $args['has_rtl'] ) {
			wp_style_add_data( $handle, 'rtl', 'replace' );
		}
	}
}
