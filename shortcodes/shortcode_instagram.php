<?php

namespace VNH\Framework\Shortcodes;

use VNH\Framework\Base;

class Shortcode_Instagram {
	public static $defaults;

	public function __construct() {
		self::$defaults = [
			'transient_name' => 'vnh_shortcode_instagram',
			'size'           => 'large',
			'username'       => 'unsplash',
			'number'         => 6,
			'before'         => '<ul class="instagram__photos" data-size="%s">',
			'after'          => '</ul>',
			'content'        => '<li class="instagram__photo"><a href="%1$s" rel="nofollow" target="_blank" ><img src="%2$s" alt="%3$s" /></a></li>',
		];
		self::$defaults = apply_filters( 'vnh/f/shortcode/instagram/defaults', self::$defaults );

		add_shortcode( 'instagram', [ $this, 'create_shortcode' ] );
	}

	public function create_shortcode( $atts ) {
		$atts = shortcode_atts( self::$defaults, $atts );

		$html = sprintf( $atts['before'], esc_attr( $atts['size'] ) );

		foreach ( $this->get_photos( $atts ) as $photo ) {
			$html .= sprintf( $atts['content'], esc_url( $photo['link'] ), esc_url( $photo[ $atts['size'] ] ), esc_html( $photo['description'] ) );
		}

		$html .= $atts['after'];

		echo $html; // WPCS XSS ok
	}

	private function get_photos( $atts ) {
		$cached_images = get_transient( $atts['transient_name'] );

		// If the photos is cached, use them & early exit.
		if ( ! empty( $cached_images ) ) {
			return $cached_images;
		}

		$username = trim( strtolower( $atts['username'] ) );

		switch ( substr( $username, 0, 1 ) ) {
			case '#':
				$url = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $username );
				break;
			default:
				$url = 'https://instagram.com/' . str_replace( '@', '', $username );
				break;
		}

		$remote = wp_remote_get( $url );

		if ( is_wp_error( $remote ) ) {
			return new \WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'vnh' ) );
		}

		if ( wp_remote_retrieve_response_code( $remote ) !== 200 ) {
			return new \WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'vnh' ) );
		}

		$shards      = explode( 'window._sharedData = ', $remote['body'] );
		$insta_json  = explode( ';</script>', $shards[1] );
		$insta_array = json_decode( $insta_json[0], true );

		if ( ! $insta_array ) {
			return new \WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'vnh' ) );
		}

		if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
			$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
		} elseif ( isset( $insta_array['entry_data']['TagPage'][0]['tag']['media']['nodes'] ) ) {
			$images = $insta_array['entry_data']['TagPage'][0]['tag']['media']['nodes'];
		} else {
			return new \WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'vnh' ) );
		}

		if ( ! is_array( $images ) ) {
			return new \WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'vnh' ) );
		}

		$photos_array = [];

		foreach ( $images as $image ) {
			$image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
			$image['display_src']   = preg_replace( '/^https?\:/i', '', $image['display_src'] );

			// handle both types of CDN url.
			if ( ( strpos( $image['thumbnail_src'], 's640x640' ) !== false ) ) {
				$image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
				$image['small']     = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
			} else {
				$url_parts  = wp_parse_url( $image['thumbnail_src'] );
				$path_parts = explode( '/', $url_parts['path'] );
				array_splice( $path_parts, 3, 0, [ 's160x160' ] );
				$image['thumbnail'] = '//' . $url_parts['host'] . implode( '/', $path_parts );
				$path_parts[3]      = 's320x320';
				$image['small']     = '//' . $url_parts['host'] . implode( '/', $path_parts );
			}

			$photos_array[] = [
				'description' => ! empty( $image['caption'] ) ? $image['caption'] : __( 'Instagram Image', 'vnh' ),
				'link'        => trailingslashit( '//instagram.com/p/' . $image['code'] ),
				'time'        => $image['date'],
				'comments'    => $image['comments']['count'],
				'likes'       => $image['likes']['count'],
				'thumbnail'   => $image['thumbnail'],
				'small'       => $image['small'],
				'large'       => $image['thumbnail_src'],
				'original'    => $image['display_src'],
				'type'        => 'image',
			];
		}

		if ( empty( $photos_array ) ) {
			return new \WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'vnh' ) );
		}

		$photos_array = array_slice( $photos_array, 0, $atts['number'] );

		set_transient( $atts['transient_name'], $photos_array, apply_filters( 'vnh/f/shortcode/instagram/cache/expiration', Base::SHORT_EXPIRATION ) ); // Cache for 1 hour using transients.

		return $photos_array;
	}
}
