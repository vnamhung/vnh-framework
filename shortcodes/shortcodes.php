<?php

namespace VNH\Framework\Shortcodes;

class Shortcodes {
	public function __construct() {
		new Shortcode_Recent_Posts();
		new Shortcode_Popular_Posts();
		new Shortcode_Instagram();
		new Shortcode_Twitter();

		add_shortcode( 'grid', [ $this, 'create_grid_shortcode' ] );
	}

	public function create_grid_shortcode( $atts, $content = null ) {
		$defaults = [
			'breakpoint' => 'lg',
			'columns'     => 1,
		];
		$atts     = shortcode_atts( $defaults, $atts );

		return sprintf( '<div class="grid-%s-%s">%s</div>', $atts['breakpoint'], $atts['columns'], do_shortcode( $content ) );
	}
}
