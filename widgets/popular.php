<?php
/**
 * Popular Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

use VNH\Framework\Like\Like;

class Popular extends Widget {
	public $display;

	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Popular Post', 'vnh' ),
				'classname'   => 'widget-popular',
				'description' => esc_html__( 'Display popular post based on number of comments or number of like.', 'vnh' ),
				'fields'      => [
					'title'           => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Popular', 'vnh' ),
					],
					'number_of_posts' => [
						'title'   => __( 'Number of posts:', 'vnh' ),
						'type'    => 'number',
						'options' => [
							'min' => 1,
							'max' => 10,
						],
						'default' => 5,
					],
					'thumbnail'       => [
						'title'   => __( 'Enable thumbnail', 'vnh' ),
						'type'    => 'dropdown',
						'options' => [
							'yes' => 'Yes',
							'no'  => 'No',
						],
						'default' => 'no',
					],
					'sort'            => [
						'title'   => __( 'Sort based on:', 'vnh' ),
						'type'    => 'dropdown_with_status',
						'options' => [
							'comment' => [
								'name'   => 'Comment count',
								'enable' => true,
							],
							'like'    => [
								'name'   => 'Like count',
								'enable' => Like::$status ? : false,
							],
						],
						'default' => 'comment',
					],
				],
				'display'     => [
					'before'  => '<ul class="popular__posts">',
					'after'   => '</ul>',
					'content' => '<li class="popular__post">%4$s<div class="popular__inner"><a class="popular__permalink" href="%1$s">%2$s</a><span>( %3$s )</span></div></li>',
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		do_shortcode(
			sprintf(
				'[popular_posts number_of_posts = %s thumbnail = %s sort = %s before=\'%s\' after=\'%s\' content=\'%s\' ]',
				$instance['number_of_posts'],
				$instance['thumbnail'],
				$instance['sort'],
				$this->display['before'],
				$this->display['after'],
				$this->display['content']
			)
		);

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance                    = $old_instance;
		$instance['title']           = sanitize_text_field( $new_instance['title'] );
		$instance['number_of_posts'] = ( 0 !== (int) $new_instance['number_of_posts'] ) ? (int) $new_instance['number_of_posts'] : null;
		$instance['thumbnail']       = sanitize_text_field( $new_instance['thumbnail'] );
		$instance['sort']            = sanitize_text_field( $new_instance['sort'] );

		return $instance;
	}
}
