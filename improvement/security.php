<?php

namespace VNH\Framework\Improvement;

use VNH\Framework\Base;
use function VNH\Framework\is_dev;

class Security {
	public static $security;

	public function __construct() {
		$defaults       = [
			'clean_up_head'     => true,
			'prevent_bad_query' => true,
		];
		self::$security = apply_filters( 'vnh/f/security', Base::$security );
		self::$security = wp_parse_args( (array) self::$security, $defaults );

		add_action( 'after_setup_theme', [ $this, 'clean_up_head' ] );
		add_action( 'wp_loaded', [ $this, 'prevent_bad_query_strings' ] );

		// Remove suffix version.
		add_filter( 'style_loader_src', [ $this, 'remove_wp_ver_css_js' ], 9999 );
		add_filter( 'script_loader_src', [ $this, 'remove_wp_ver_css_js' ], 9999 );

		add_filter( 'wp_default_scripts', [ $this, 'dequeue_jquery_migrate' ] );
	}

	public function clean_up_head() {
		if ( empty( self::$security['clean_up_head'] ) ) {
			return;
		}

		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'wp_shortlink_wp_head' );

		// Disable XMLRPC
		add_filter( 'xmlrpc_enabled', '__return_false' );
		add_filter( 'pre_option_enable_xmlrpc', '__return_false' );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );

		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );

		add_filter( 'the_generator', '__return_false' );

		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
	}

	public function prevent_bad_query_strings() {
		if ( empty( self::$security['prevent_bad_query'] ) ) {
			return;
		}

		if ( array_key_exists( 'modTest', $_GET ) ) {
			header( 'HTTP/1.1 403 Unauthorized' );
			wp_die( 'Error 403: Forbidden.' );
		}
	}

	public function remove_wp_ver_css_js( $src ) {
		if ( is_dev() ) {
			return $src;
		}

		return strpos( $src, 'ver' ) ? remove_query_arg( 'ver', $src ) : $src;
	}

	public function dequeue_jquery_migrate( \WP_Scripts &$scripts ) {
		if ( ! is_dev() && ! is_admin() ) {
			return;
		}

		$scripts->remove( 'jquery' );
		$scripts->add( 'jquery', false, [ 'jquery-core' ], '1.10.2' );
	}
}
