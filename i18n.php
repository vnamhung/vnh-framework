<?php

namespace VNH\Framework;

class I18n {
	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'load_textdomains' ] );

		// Load framework textdomains
		add_filter( 'override_load_textdomain', [ $this, 'framework_textdomains' ], 10, 2 );
	}

	public function load_textdomains() {
		unload_textdomain( 'acf' );
		unload_textdomain( 'pt-ocdi' );
		unload_textdomain( 'plugin-update-checker' );

		// Loads wpvn-default-vi.mo in wp-content/languages/themes
		load_theme_textdomain( Base::$cached['text_domain'], trailingslashit( WP_LANG_DIR ) . 'themes' );

		// Loads wpvn-default-vi.mo in wp-content/themes/child-theme-name/languages
		if ( is_child_theme() ) {
			load_theme_textdomain( Base::$cached['child_text_domain'], Base::$cached['child_domain_directory'] );
		}

		// Loads wpvn-default-vi.mo in wp-content/themes/wpvn-default/languages
		load_theme_textdomain( Base::$cached['text_domain'], Base::$cached['domain_directory'] );

		// Load the framework textdomains.
		load_textdomain( 'kirki', '' );
		load_textdomain( 'acf', '' );
		load_textdomain( 'pt-ocdi', '' );
		load_textdomain( 'plugin-update-checker', '' );
	}

	public function framework_textdomains( $override, $domain ) {
		// Check if the domain is our framework domain.
		if ( $domain === 'kirki' || $domain === 'acf' || $domain === 'pt-ocdi' || $domain === 'plugin-update-checker' ) {
			global $l10n;
			// If the theme's textdomain is loaded, assign the theme's translations
			// to the framework's textdomain.
			if ( isset( $l10n[ Base::$cached['text_domain'] ] ) ) {
				$l10n[ $domain ] = $l10n[ Base::$cached['text_domain'] ]; // WPCS: override ok.
			}
			// Always override.  We only want the theme to handle translations.
			$override = true;
		}

		return $override;
	}
}
