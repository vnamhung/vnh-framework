<?php

namespace VNH\Framework\Social;

use VNH\Framework\Base;
use VNH\Framework\Helper;

class Social_Share {
	public static $default_social_networks = [
		'facebook'    => true,
		'twitter'     => true,
		'google-plus' => true,
		'pinterest'   => true,
		'linkedin'    => true,
		'tumblr'      => true,
		'vk'          => true,
	];

	public static $social_networks;

	public function __construct() {
		self::$social_networks = apply_filters( 'vnh/f/social_share/networks', Base::$social_networks );
		self::$social_networks = wp_parse_args( (array) self::$social_networks, self::$default_social_networks );

		add_action( 'vnh/h/social_share', [ $this, 'the_social_share' ] );
	}

	public static function the_social_share() {
		echo self::get_social_share(); // WPCS XSS ok
	}

	public static function get_social_share( $before = '<div class="social-share">', $after = '</div>' ) {
		$html = $before;

		foreach ( self::$social_networks as $net => $status ) :
			if ( $status ) {
				$thumbnail = esc_url( wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] );

				$html .= sprintf(
					'<button class="hint--top" aria-label="Share on %s" onclick="%s">%s</button>',
					ucfirst( str_replace( '-', ' ', $net ) ),
					self::get_social_network_share_link( $net, $thumbnail ),
					Helper::get_svg_icon( [ 'icon' => $net ] )
				);
			}

		endforeach;

		$html .= $after;

		return $html;
	}

	private static function get_social_network_share_link( $net, $thumbnail ) {
		switch ( $net ) {
			case 'facebook':
				if ( wp_is_mobile() ) {
					$link = 'window.open(\'http://m.facebook.com/sharer.php?u=' . rawurlencode( get_permalink() ) . '\');';
				} else {
					$link = 'window.open(\'http://www.facebook.com/sharer.php?s=100&amp;p[title]=' . rawurlencode( get_the_title() ) . '&amp;p[url]=' . rawurlencode( get_permalink() ) . '&amp;p[images][0]=' . $thumbnail . '&amp;p[summary]=' . rawurlencode( get_the_excerpt() ) . '\', \'sharer\', \'toolbar=0,status=0,width=620,height=280\');';
				}
				break;
			case 'twitter':
				$link = 'window.open(\'https://twitter.com/share?url=' . rawurlencode( get_permalink() ) . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');';
				break;
			case 'google-plus':
				$link = 'popUp=window.open(\'https://plus.google.com/share?url=' . rawurlencode( get_permalink() ) . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');popUp.focus();return false;';
				break;
			case 'linkedin':
				$link = 'popUp=window.open(\'http://linkedin.com/shareArticle?mini=true&amp;url=' . rawurlencode( get_permalink() ) . '&amp;title=' . rawurlencode( get_the_title() ) . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');popUp.focus();return false;';
				break;
			case 'tumblr':
				$link = 'popUp=window.open(\'http://www.tumblr.com/share/link?url=' . rawurlencode( get_permalink() ) . '&amp;name=' . rawurlencode( get_the_title() ) . '&amp;description=' . rawurlencode( get_the_excerpt() ) . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');popUp.focus();return false;';
				break;
			case 'pinterest':
				$link = 'popUp=window.open(\'http://pinterest.com/pin/create/button/?url=' . rawurlencode( get_permalink() ) . '&amp;description=' . get_the_title() . '&amp;media=' . rawurlencode( $thumbnail[0] ) . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');popUp.focus();return false;';
				break;
			case 'vk':
				$link = 'popUp=window.open(\'http://vkontakte.ru/share.php?url=' . rawurlencode( get_permalink() ) . '&amp;title=' . rawurlencode( get_the_title() ) . '&amp;description=' . rawurlencode( get_the_excerpt() ) . '&amp;image=' . rawurlencode( $thumbnail[0] ) . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');popUp.focus();return false;';
				break;
			default:
				$link = '';
		}

		return $link;

	}
}
