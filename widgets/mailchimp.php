<?php
/**
 * Mailchimp Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

class Mailchimp extends Widget {
	public $form_texts;

	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Mailchimp', 'vnh' ),
				'classname'   => 'widget-mailchimp',
				'description' => esc_html__( 'Display a subscribe form for collecting emails with MailChimp.', 'vnh' ),
			]
		);

		$this->set_params();

		// AJAX callback.
		add_action( 'wp_ajax_pt_mailchimp_subscribe_get_lists', [ $this, 'mailchimp_get_lists' ] );
	}

	protected function set_params() {
		$this->defaults = [
			'title'         => esc_html__( 'Newsletter', 'vnh' ),
			'api_key'       => '',
			'account_id'    => '',
			'selected_list' => '',
		];
		$this->defaults = apply_filters( 'vnh/f/widget_mailchimp/defaults', $this->defaults );

		$this->form_texts = [
			'email'  => esc_html__( 'Your E-mail Address', 'vnh' ),
			'submit' => esc_html__( 'Subscribe!', 'vnh' ),
		];
		$this->form_texts = apply_filters( 'vnh/f/widget_mailchimp/form_texts', $this->form_texts );
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		$this->display_form( $instance );

		self::after_widget_content( $args );
	}

	public function display_form( $instance ) {
		if ( ! empty( $instance['api_key'] ) && preg_match( '/us\d{1,2}$/', $instance['api_key'], $mc_dc_match ) ) {
			$mc_datacenter = $mc_dc_match[0];
			$url           = '//github.%1$s.list-manage.com/subscribe/post?u=%2$s&amp;id=%3$s';
			$form_action   = sprintf( $url, esc_attr( $mc_datacenter ), esc_attr( $instance['account_id'] ), esc_attr( $instance['selected_list'] ) );
		}

		$action          = ! empty( $form_action ) ? "action='$form_action'" : '';
		$security_string = sprintf( 'b_%1$s_%2$s', esc_attr( $instance['account_id'] ), esc_attr( $instance['selected_list'] ) );
		$email_input     = sprintf( '<input type="email" value="" name="EMAIL" class="email form-control mailchimp-subscribe__email-input" placeholder="%s" required>', esc_html( $this->form_texts['email'] ) );
		$hidden_input    = sprintf( '<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="%s" tabindex="-1" value=""></div>', esc_attr( $security_string ) );
		$submit_input    = sprintf( '<input type="submit" value="%s" name="subscribe" class="mailchimp-subscribe__submit">', esc_html( $this->form_texts['submit'] ) );
		$form            = '<form %1$s method="post" name="mailchimp-embedded-subscribe-form" class="mailchimp-subscribe validate" target="_blank" novalidate>%2$s %3$s %4$s</form>';

		printf( $form, $action, $email_input, $hidden_input, $submit_input ); // WPCS XSS ok
	}

	public function update( $new_instance, $old_instance ) {
		$instance                  = $old_instance;
		$instance['title']         = sanitize_text_field( $new_instance['title'] );
		$instance['api_key']       = sanitize_text_field( $new_instance['api_key'] );
		$instance['account_id']    = sanitize_text_field( $new_instance['account_id'] );
		$instance['selected_list'] = sanitize_text_field( $new_instance['selected_list'] );

		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		printf(
			'<p><label for="%1$s">%2$s</label><input class="widefat" id="%1$s" name="%3$s" type="text" value="%4$s"></p>',
			esc_attr( $this->get_field_id( 'title' ) ),
			esc_html__( 'Title:', 'vnh' ),
			esc_attr( $this->get_field_name( 'title' ) ),
			esc_attr( $instance['title'] )
		);

		$guide = esc_html__( 'In order to use this widget, you have to: ', 'vnh' );
		$li1   = sprintf( esc_html__( '%1$sVisit this URL and login with your mailchimp account%2$s', 'vnh' ), '<li><a href="https://admin.mailchimp.com/account/api" target="_blank">', '</a></li>' );
		$li2   = sprintf( esc_html__( '%1$sCreate an API key%2$s and paste it in the input field below%3$s', 'vnh' ), '<li><a href="http://kb.mailchimp.com/integrations/api-integrations/about-api-keys#Find-or-Generate-Your-API-Key" target="_blank">', '</a>', '</li>' );
		$li3   = sprintf( esc_html__( '%1$sClick on the Connect button, so that your existing MailChimp lists can be retrieved,%2$s', 'vnh' ), '<li>', '</li>' );
		$li4   = sprintf( esc_html__( '%1$sSelect which list you want your visitors to subscribe to, from the dropdown menu below.%2$s', 'vnh' ), '<li>', '</li>' );

		printf( '%s<ol>%s %s %s %s</ol>', $guide, $li1, $li2, $li3, $li4 ); // WPCS XSS OK

		$label  = sprintf( '<label for="%s">%s</label>', esc_attr( $this->get_field_id( 'api_key' ) ), esc_html__( 'MailChimp API key:', 'vnh' ) );
		$input1 = sprintf( '<input class="js-mailchimp-api-key" id="%s" name="%s" type="text" value="%s"/>', esc_attr( $this->get_field_id( 'api_key' ) ), esc_attr( $this->get_field_name( 'api_key' ) ), esc_attr( $instance['api_key'] ) );
		$input2 = sprintf( '<input class="js-connect-mailchimp-api-key  button" type="button" value="%s">', esc_html__( 'Connect', 'vnh' ) );
		$input3 = sprintf( '<input class="js-mailchimp-account-id" id="%s" name="%s" type="hidden" value="%s">', esc_attr( $this->get_field_id( 'api_key' ) ), esc_attr( $this->get_field_name( 'account_id' ) ), esc_attr( $instance['account_id'] ) );
		printf( '<p>%s %s %s %s</p>', $label, $input1, $input2, $input3 ); // WPCS XSS ok

		printf( '<p class="js-mailchimp-loader" style="display: none;"><span class="spinner" style="display: inline-block; float: none; visibility: visible; margin-bottom: 6px;"></span></p>', esc_html__( 'Loading ...', 'vnh' ) );

		echo '<div class="js-mailchimp-notice"></div>';

		$label  = sprintf( '<label for="%s">%s</label>', esc_attr( $this->get_field_id( 'list' ) ), esc_html__( 'MailChimp list:', 'vnh' ) );
		$select = sprintf( '<select id="%s" name="%s"></select>', esc_attr( $this->get_field_id( 'list' ) ), esc_attr( $this->get_field_name( 'list' ) ) );
		$input  = sprintf( '<input class="js-mailchimp-selected-list" id="%s" name="%s" type="hidden" value="%s">', esc_attr( $this->get_field_id( 'selected_list' ) ), esc_attr( $this->get_field_name( 'selected_list' ) ), esc_attr( $instance['selected_list'] ) );

		printf( '<p class="js-mailchimp-list-container" style="display: none;">%s <br> %s %s</p>', $label, $select, $input ); // WPCS XSS ok
	}

	/**
	 * AJAX callback function to retrieve the MailChimp lists.
	 */
	public function mailchimp_get_lists() {
		check_ajax_referer( 'pt-mcw-ajax-verification', 'security' );

		$response = [];

		$api_key       = sanitize_text_field( $_GET['api_key'] );
		$mc_datacenter = sanitize_text_field( $_GET['mc_dc'] );

		$args = [
			'headers' => [
				'Authorization' => sprintf( 'apikey %1$s', $api_key ),
			],
		];

		$mc_lists_endpoint = sprintf( 'https://%1$s.api.mailchimp.com/3.0/lists', $mc_datacenter );

		$request = wp_remote_get( $mc_lists_endpoint, $args );

		// Error while connecting to the MailChimp server.
		if ( is_wp_error( $request ) ) {
			$response['message'] = esc_html__( 'There was an error connecting to the MailChimp servers.', 'vnh' );

			wp_send_json_error( $response );
		}

		// Retrieve the response code and body.
		$response_code = wp_remote_retrieve_response_code( $request );
		$response_body = json_decode( wp_remote_retrieve_body( $request ), true );

		// The request was not successful.
		if ( 200 !== $response_code ) {
			$response['message'] = sprintf( esc_html__( 'Error: %1$s (error code: %2$s)', 'vnh' ), $response_body['title'], $response_body['status'] );

			wp_send_json_error( $response );
		}

		// There are no lists in this MailChimp account.
		if ( empty( $response_body['lists'] ) ) {
			$response['message'] = esc_html__( 'There are no email lists with this API key! Please create an email list in the MailChimp dashboard and try again.', 'vnh' );

			wp_send_json_error( $response );
		}

		$mc_account_id = $this->get_mailchimp_account_id( $api_key, $mc_datacenter );

		if ( empty( $mc_account_id ) ) {
			$response['message'] = esc_html__( 'There was an error connecting to the MailChimp servers.', 'vnh' );

			wp_send_json_error( $response );
		}

		$lists = [];

		// Parse through the retrieved lists and collect the info we need.
		foreach ( $response_body['lists'] as $list ) {
			$lists[ $list['id'] ] = $list['name'];
		}

		$response['message']    = esc_html__( 'MailChimp lists were successfully retrieved!', 'vnh' );
		$response['lists']      = $lists;
		$response['account_id'] = $mc_account_id;

		wp_send_json_success( $response );
	}

	private function get_mailchimp_account_id( $api_key, $mc_datacenter ) {
		$args = [
			'headers' => [
				'Authorization' => sprintf( 'apikey %1$s', $api_key ),
			],
		];

		$mc_account_endpoint = sprintf( 'https://%1$s.api.mailchimp.com/3.0/', $mc_datacenter );

		$request = wp_remote_get( $mc_account_endpoint, $args );

		if ( is_wp_error( $request ) || 200 !== wp_remote_retrieve_response_code( $request ) ) {
			return false;
		}

		$body = json_decode( wp_remote_retrieve_body( $request ), true );

		return $body['account_id'];
	}
}
