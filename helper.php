<?php

namespace VNH\Framework;

use VNH\Framework\Kirki\Kirki_Wrapper;

function is_dev() {
	return defined( 'DEV_ENV' ) && DEV_ENV;
}

class Helper {
	public static function the_youtube_iframe( $iframe, $lazy_load = true, $youtube_params = [] ) {
		if ( $lazy_load ) {
			$iframe = str_replace( 'src', 'data-src', $iframe ); //lazyload

			// use preg_match to find iframe src
			preg_match( '/data-src="(.+?)"/', $iframe, $matches );
		} else {
			preg_match( '/src="(.+?)"/', $iframe, $matches );
		}

		$src = $matches[1];

		// add extra params to iframe src
		$default_params = [
			'controls' => 0,
			'showinfo' => 0,
			'color'    => 'white',
			'hd'       => 1,
			'rel'      => 0,
		];
		$youtube_params = wp_parse_args( (array) $youtube_params, $default_params );

		$new_src = add_query_arg( $youtube_params, $src );

		$iframe = str_replace( $src, $new_src, $iframe );

		// add extra attributes to iframe html
		$attributes = 'frameborder="0"';

		$iframe = str_replace( '></iframe>', ' ' . $attributes . '></iframe>', $iframe );

		echo $iframe; // WPCS XSS ok
	}

	public static function fragment_cache( $transient_name, $function, $expiration = 1 ) {
		if ( is_user_logged_in() ) {
			call_user_func( $function );

			return;
		}

		$transient_name = Base::PREFIX . '_' . $transient_name;
		$output         = get_transient( $transient_name );

		if ( $output === false ) {
			ob_start();
			call_user_func( $function );
			$output = ob_get_clean();
			$output = gzcompress( $output, 9 );
			$output = base64_encode( $output ); //phpcs:disable

			set_transient( $transient_name, $output, $expiration * HOUR_IN_SECONDS );
		}

		echo gzuncompress( base64_decode( $output ) ); //phpcs:disable
	}

	public static function get_widget_field( $field_name, $args ) {
		return get_field( $field_name, 'widget_' . $args['widget_id'] );
	}

	public static function the_related_posts( $args = [] ) {
		$default_args = [
			'post_id'       => get_the_ID(),
			'number_posts'  => 3,
			'template_slug' => 'template-parts/_archive/content',
			'template_name' => get_post_format(),
			'before'        => '',
			'after'         => '',
		];
		$args         = wp_parse_args( (array) $args, $default_args );

		$query_params = [
			'ignore_sticky_posts' => 0,
			'category__in'        => wp_get_post_categories( $args['post_id'] ),
			'posts_per_page'      => $args['number_posts'],
			'post__not_in'        => [ $args['post_id'] ],
		];

		if ( $args['number_posts'] === 0 ) {
			$related_posts = new \WP_Query();
		} else {
			$related_posts = new \WP_Query( $query_params );
		}

		if ( $related_posts->have_posts() ) :
			echo $args['before'];

			while ( $related_posts->have_posts() ) :
				$related_posts->the_post();

				get_template_part( $args['template_slug'], $args['template_name'] );
			endwhile;

			wp_reset_postdata();

			echo $args['after'];
		endif;
	}

	public static function the_cpt_related_posts( $args = [] ) {
		$default_args = [
			'post_type'     => 'post',
			'tax'           => 'category',
			'number_posts'  => 3,
			'post_id'       => get_the_ID(),
			'template_slug' => 'template-parts/_archive/content',
			'template_name' => get_post_format(),
			'before'        => '',
			'after'         => '',
		];
		$args         = wp_parse_args( (array) $args, $default_args );

		$item_cats = get_the_terms( $args['post_id'], $args['tax'] );

		$item_array = [];
		if ( $item_cats ) {
			foreach ( $item_cats as $item_cat ) {
				$item_array[] = $item_cat->term_id;
			}
		}

		if ( empty( $item_array ) ) {
			return null;
		}

		$query_params = [
			'ignore_sticky_posts' => 0,
			'posts_per_page'      => $args['number_posts'],
			'post__not_in'        => [ $args['post_id'] ],
			'post_type'           => $args['post_type'],
			'tax_query'           => [
				[
					'field'    => 'id',
					'taxonomy' => $args['tax'],
					'terms'    => $item_array,
				],
			],
		];

		if ( $args['number_posts'] === 0 ) {
			$related_posts = new \WP_Query();
		} else {
			$related_posts = new \WP_Query( $query_params );
		}

		if ( $related_posts->have_posts() ) :
			echo $args['before'];

			while ( $related_posts->have_posts() ) :
				$related_posts->the_post();

				get_template_part( $args['template_slug'], $args['template_name'] );
			endwhile;

			wp_reset_postdata();

			echo $args['after'];
		endif;
	}

	public static function get_kirki_option( $setting ) {
		return Kirki_Wrapper::get_option( Base::$cached['theme_slug'], $setting );
	}

	public static function get_gg_fonts_url( $args = [] ) {
		$defaults = [
			'subsets' => 'latin,latin-ext',
		];

		$args  = wp_parse_args( $args, $defaults );
		$fonts = [];

		foreach ( $args['font-families'] as $family => $params ) :
			$font_weight = $params['font-weight'];

			if ( $params['translate'] === true ) {
				$fonts[] = "$family:$font_weight";
			}
		endforeach;

		if ( empty( $fonts ) ) {
			return null;
		}

		$fonts_url = add_query_arg(
			[
				'family' => rawurlencode( implode( '|', $fonts ) ),
				'subset' => rawurlencode( $args['subsets'] ),
			],
			'https://fonts.googleapis.com/css'
		);

		return $fonts_url;
	}

	public static function get_img( $args ) {
		$defaults = [
			'img_ID'       => get_post_thumbnail_id( $GLOBALS['post'] ),
			'crop'         => [
				'width'  => '',
				'height' => '',
				'ratio'  => 0.1,
			],
			'custom_class' => 'lazy',
			'before'       => '<div class="lazy-wrapper">',
			'after'        => '</div>',
		];
		$args     = wp_parse_args( (array) $args, $defaults );

		$id                 = $args['img_ID'];
		$custom_class       = $args['custom_class'];
		$crop               = $args['crop'];
		$ratio              = $crop['ratio'];
		$crop_width         = $crop['width'];
		$crop_height        = $crop['height'];
		$crop_width_retina  = $crop['width'] * 2;
		$crop_height_retina = $crop['height'] * 2;

		$full_img_url = wp_get_attachment_image_url( $id, 'full' );

		list( $full_width, $full_height ) = getimagesize( $full_img_url );

		$uploads_dir          = wp_upload_dir();
		$svg_placeholder_url  = preg_replace( '/\.(jpe?g|png|gif|bmp)$/i', '.svg', $full_img_url );
		$svg_placeholder_path = str_replace( $uploads_dir['baseurl'], $uploads_dir['basedir'], $svg_placeholder_url );

		if ( file_exists( $svg_placeholder_path ) ) {
			$svg_content = self::get_file_content( $svg_placeholder_url );
			$svg_content = str_replace( '#', '%23', $svg_content ); // fix Data-URI SVG not working in Firefox: # in the content needs to be escaped as %23.
			$placeholder = 'data:image/svg+xml;utf8,' . $svg_content;
		} else {
			if ( empty( $crop ) || empty( $crop_width ) || empty( $crop_height ) ) {
				$img_placeholder_size = [ $full_width * $ratio, $full_height * $ratio ];
			} else {
				$img_placeholder_size = [ $crop_width * $ratio, $crop_height * $ratio ];
			}

			$placeholder = wp_get_attachment_image_url( $id, $img_placeholder_size );
		}

		if ( empty( $crop ) || empty( $crop_width ) || empty( $crop_height ) ) {
			$normal_img_url = $full_img_url;
			$width          = $full_width;
			$height         = $full_height;
		} else {
			$normal_img_url = wp_get_attachment_image_url( $id, [ $crop_width, $crop_height ] );
			$width          = $crop_width;
			$height         = $crop_height;
		}

		if ( ! empty( $crop_width ) ) {
			if ( $full_width > $crop_width_retina ) {
				$retina_img_url = wp_get_attachment_image_url( $id, [ $crop_width_retina, $crop_height_retina ] );
			} elseif ( $full_width === $crop_width_retina ) {
				$retina_img_url = $full_img_url;
			} else {
				$retina_img_url = null;
			}
		}

		$retina_img_url = ! empty( $retina_img_url ) ? "$retina_img_url 2x" : '';

		$alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

		$img = $args['before'];
		$img .= "<img class='$custom_class' src='$placeholder' data-srcset='$normal_img_url 1x, $retina_img_url' alt='$alt' width='$width' height='$height' />";
		$img .= $args['after'];

		return $img;
	}

	/**
	 * Returns true if a blog has more than 1 category.
	 *
	 * @return bool
	 */
	public static function categorized_blog() {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( [
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		] );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		return $all_the_cool_cats > 1 ? true : false;
	}

	public static function get_featured_image_class() {
		$url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );

		list( $width, $height ) = getimagesize( $url );

		$content = get_the_content();

		if ( $width > $height || $width === $height ) {
			$thumb_class = 'featured-landscape';
		} elseif ( empty( $content ) ) {
			$thumb_class = 'featured-no-content';
		} else {
			$thumb_class = 'featured-portrait';
		}

		return $thumb_class;
	}

	public static function the_custom_excerpt( $words_number ) {
		$ending = get_option( 'wl_excerpt_ending' );
		$limit  = $words_number + 1;

		$excerpt = explode( ' ', get_the_excerpt(), $limit );
		array_pop( $excerpt );
		$excerpt = implode( ' ', $excerpt ) . $ending;

		printf( '<p>%s</p>', wp_kses_post( $excerpt ) );

		if ( empty( $read_more ) ) {
			return;
		}

		printf( '<a class="read-more" href="%s">%s</a>', esc_url( get_permalink() ), wp_kses_post( get_option( 'wl_readmore_link' ) ) );
	}

	public static function convert_to_snake_string( $input, $after = '', $before = '' ) {
		$output = str_replace( [ '\\', '/' ], '_', $input );
		$output = strtolower( $output );
		$output = $before . $output . $after;

		return $output;
	}

	public static function convert_to_hyphen_string( $input, $after = '', $before = '' ) {
		$output = str_replace( [ '\\', '/' ], '-', $input );
		$output = strtolower( $output );
		$output = $before . $output . $after;

		return $output;
	}

	public static function scan_files( $dir, $loop = false ) {
		$files  = @scandir( $dir );
		$result = [];

		if ( ! empty( $files ) ) {
			foreach ( $files as $key => $value ) {
				if ( ! in_array( $value, [ '.', '..' ], true ) ) {
					if ( is_dir( $dir . DIRECTORY_SEPARATOR . $value ) && $loop === true ) {
						$sub_files = self::scan_files( $dir . DIRECTORY_SEPARATOR . $value );
						foreach ( $sub_files as $sub_file ) {
							$result[] = $value . DIRECTORY_SEPARATOR . $sub_file;
						}
					} else {
						$result[] = $value;
					}
				}
			}
		}

		return $result;
	}

	public static function init_filesystem() {
		global $wp_filesystem;

		if ( empty( $wp_filesystem ) ) {
			require_once ABSPATH . '/wp-admin/includes/file.php';
			WP_Filesystem();
		}

		return $wp_filesystem;
	}

	public static function get_file_content( $file ) {
		global $wp_filesystem;
		self::init_filesystem();

		$file_content = $wp_filesystem->get_contents( $file );

		return $file_content;
	}

	/**
	 * Don't use the WP_Filesystem methods because $wp_filesystem->get_contents( $file )
	 * read the entire file while we only read first 8kiB of file.
	 * In terms of performance I think it would be unnecessary to use it...
	 * https://github.com/woocommerce/woocommerce/issues/6091
	 *
	 * @param $file
	 *
	 * @return string
	 */
	public static function get_file_version( $file ) {
		// Avoid notices if file does not exist
		if ( ! file_exists( $file ) ) {
			return '';
		}

		// We don't need to write to the file, so just open for reading.
		$fp = fopen( $file, 'r' );

		// Pull only the first 8kiB of the file in.
		$file_data = fread( $fp, 8192 );

		// PHP will close file handle, but we are good citizens.
		fclose( $fp );

		// Make sure we catch CR-only line endings.
		$file_data = str_replace( "\r", "\n", $file_data );
		$version   = '';

		if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( '@version', '/' ) . '(.*)$/mi', $file_data, $match ) && $match[1] ) {
			$version = _cleanup_header_comment( $match[1] );
		}

		return $version;
	}

	public static function human_time_diff_maybe( $timestamp, $day = 1 ) {
		$how_long = DAY_IN_SECONDS * $day;

		if ( ( abs( time() - $timestamp ) ) < $how_long ) {
			return sprintf( __( '%1$s ago', 'vnh' ), human_time_diff( $timestamp ) );
		} else {
			return date( get_option( 'date_format' ), $timestamp );
		}
	}

	public static function the_svg_icon( $icon ) {
		$args = [ 'icon' => $icon ];

		echo wp_kses( self::get_svg_icon( $args ), self::svg_allowed_html() );
	}

	public static function get_svg_icon( $args = [] ) {
		$defaults = apply_filters( 'vnh/f/svg/defaults', [
			'icon'        => '',
			'title'       => '',
			'desc'        => '',
			'aria_hidden' => true, // Hide from screen readers.
		] );

		$args            = wp_parse_args( $args, $defaults );
		$aria_hidden     = $args['aria_hidden'] === true ? 'aria-hidden="true"' : '';
		$aria_labelledby = $args['title'] && $args['desc'] ? 'aria-labelledby="title desc"' : '';

		// Begin SVG markup.
		$svg = sprintf( '<svg class="icon icon--%s" %s %s role="img">', $args['icon'], $aria_hidden, $aria_labelledby );
		$svg .= $args['title'] ? sprintf( '<title>%s</title>', $args['title'] ) : '';
		$svg .= $args['desc'] ? sprintf( '<desc>%s</desc>', $args['desc'] ) : '';
		$svg .= sprintf( '<use xlink:href="#%s"></use>', $args['icon'] );
		$svg .= '</svg>';

		return $svg;
	}

	public static function svg_allowed_html() {
		$array = [
			'svg' => [
				'class'       => [],
				'aria-hidden' => [],
				'role'        => [],
			],
			'use' => [
				'xlink:href' => [],
			],
		];

		return apply_filters( 'vnh/f/svg/allowed_html', $array );
	}

	public static function get_first_image() {
		$image   = '';
		$link    = get_permalink();
		$content = get_the_content();
		$count   = substr_count( $content, '<img' );
		$start   = 0;
		for ( $i = 1; $i <= $count; $i++ ) {
			$img_beg     = strpos( $content, '<img', $start );
			$post        = substr( $content, $img_beg );
			$img_end     = strpos( $post, '>' );
			$post_output = substr( $post, 0, $img_end + 1 );
			$post_output = preg_replace( '/width="([0-9]*)" height="([0-9]*)"/', '', $post_output );
			$image[ $i ] = $post_output;
			$start       = $img_end + 1;
		}

		return stristr( $image[1], '<img' ) ? "<a href=$link>$image[1]</a>" : null;
	}

	public static function flatten_version( $version ) {
		if ( empty( $version ) ) {
			return null;
		}

		$parts = explode( '.', $version );

		if ( count( $parts ) === 2 ) {
			$parts[] = '0';
		}

		return implode( '', $parts );
	}

	public static function minify_css( $css = '' ) {
		// Return if no CSS
		if ( ! $css ) {
			return null;
		}

		// Normalize whitespace
		$css = preg_replace( '/\s+/', ' ', $css );

		// Remove ; before }
		$css = preg_replace( '/;(?=\s*})/', '', $css );

		// Remove space after , : ; { } */ >
		$css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );

		// Remove space before , ; { }
		$css = preg_replace( '/ (,|;|\{|})/', '$1', $css );

		// Strips leading 0 on decimal values (converts 0.5px into .5px)
		$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );

		// Strips units if value is 0 (converts 0px to 0)
		$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );

		// Trim
		$css = trim( $css );

		// Return minified CSS
		return $css;
	}
}
