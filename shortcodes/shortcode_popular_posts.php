<?php

namespace VNH\Framework\Shortcodes;

use VNH\Framework\Base;
use VNH\Framework\Image;
use VNH\Framework\Like\Like;

class Shortcode_Popular_Posts {
	public static $defaults;

	public function __construct() {
		self::$defaults = [
			'number_of_posts' => 5,
			'thumbnail'       => 'no',
			'sort'            => 'comment',
			'before'          => '<ul class="popular__posts">',
			'after'           => '</ul>',
			'content'         => '<li class="popular__post">%4$s<div class="popular__inner"><a class="popular__permalink" href="%1$s">%2$s</a><span>( %3$s )</span></div></li>',
		];
		self::$defaults = apply_filters( 'vnh/f/shortcode/popular_posts/defaults', self::$defaults );

		add_shortcode( 'popular_posts', [ $this, 'create_shortcode' ] );
	}

	public function create_shortcode( $atts ) {
		$atts = shortcode_atts( self::$defaults, $atts );

		if ( $atts['sort'] === 'like' ) {
			$args = [
				'posts_per_page' => $atts['number_of_posts'],
				'orderby'        => 'meta_value_num',
				'order'          => 'DESC',
				'meta_key'       => '_be_like_content',
			];
		} else {
			$args = [
				'posts_per_page' => $atts['number_of_posts'],
				'orderby'        => 'comment_count',
			];
		}

		$query = new \WP_Query( $args );

		if ( $query->have_posts() ) :
			$html = $atts['before'];

			while ( $query->have_posts() ) :
				$query->the_post();

				$likes     = intval( get_post_meta( get_the_ID(), '_be_like_content', true ) );
				$likes     = ! empty( $likes ) ? $likes . _n( Like::$settings['single'], Like::$settings['plural'], $likes ) : null; //phpcs:disable
				$count     = $atts['sort'] === 'like' ? $likes : get_comments_number_text( '0' );
				$thumbnail = $atts['thumbnail'] === 'yes' ? new Image( Base::$image_sizes['popular_widget'], false ) : null;

				if ( file_exists( get_theme_file_path( 'template-parts/shortcode/popular.php' ) ) ) {
					get_template_part( 'template-parts/shortcode/popular' );
				} else {
					$html .= sprintf( $atts['content'], get_the_permalink(), get_the_title(), $count, $thumbnail );
				}
			endwhile;

			wp_reset_postdata();

			$html .= $atts['after'];

			echo $html; // WPCS XSS ok
		endif;
	}
}
