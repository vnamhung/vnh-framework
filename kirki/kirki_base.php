<?php

namespace VNH\Framework\Kirki;

use VNH\Framework\Base;

abstract class Kirki_Base {
	public static function add_custom_field( $args ) {
		$defaults = [
			'type'     => 'custom',
			'settings' => uniqid(),
		];
		$args     = wp_parse_args( $args, $defaults );

		return Kirki_Wrapper::add_field( $args );
	}

	public static function get_big_title( $title, $desc = '' ) {
		if ( empty( $desc ) ) {
			return sprintf( '<div class="big_title">%s</div>', $title );
		} else {
			return sprintf( '<div class="big_title">%s</div><div class="desc">%s</div>', $title, $desc );
		}
	}

	public static function get_group_title( $title, $icon = '' ) {
		if ( empty( $icon ) ) {
			return sprintf( '<div class="group_title">%s</div>', $title );
		} else {
			return sprintf( '<div class="group_title"><i class="dashicons %s"></i>%s</div>', $icon, $title );
		}
	}

	public static function get_layout( $layouts ) {
		$layouts_array = [];
		foreach ( $layouts as $layout ) {
			$layouts_array[ $layout ] = Base::$cached['parent_directory_uri'] . Base::ASSETS_DIR . "images/$layout.png";
		}

		return $layouts_array;
	}
}
