<?php

namespace VNH\Framework;

abstract class Customize_Base {
	public function __construct() {
		add_action( 'widgets_init', [ $this, 'load_customizer' ], 99 );
		add_action( 'customize_register', [ $this, 'remove_customizer_elements' ] );

		if ( ! empty( Base::$customize['live_edit_title_desc'] ) ) {
			add_action( 'customize_register', [ $this, 'live_edit_title_desc' ] );
			add_action( 'customize_preview_init', [ $this, 'live_edit_title_desc_js' ] );
		}
	}

	abstract public function load_customizer();
	abstract public function remove_customizer_elements( \WP_Customize_Manager $wp_customize );

	public function live_edit_title_desc_js() {
		wp_enqueue_script( 'live-title-desc', Base::$cached['widgets']['live_title_desc_js_url'], [ 'customize-preview' ], null, true );
	}

	public function live_edit_title_desc( \WP_Customize_Manager $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
	}
}
