<?php

namespace VNH\Framework\Widgets;

use VNH\Framework\Base;
use VNH\Framework\Helper;

abstract class Widget extends \WP_Widget {
	public $defaults, $fields, $display;

	public function __construct( $args = [] ) {
		if ( empty( $args ) ) {
			return;
		}

		parent::__construct(
			$args['base_id'], // Base ID
			sprintf( '%s - %s', Base::$cached['theme_name'], $args['name'] ), // Name,
			[
				'classname'   => $args['classname'],
				'description' => $args['description'],
			]
		);

		$widget_name = str_replace( '-', '_', $args['classname'] );

		if ( ! empty( $args['fields'] ) ) {
			$this->fields = $args['fields'];
			$this->fields = apply_filters( "vnh/f/$widget_name/fields", $args['fields'] );

			// Set default values
			$this->defaults = [];
			foreach ( $this->fields as $field_id => $field_args ) {
				if ( array_key_exists( 'default', $field_args ) ) {
					$this->defaults[ $field_id ] = $field_args['default'];
				}
			}
		}

		if ( ! empty( $args['display'] ) ) {
			$this->display = $args['display'];
			$this->display = apply_filters( "vnh/f/$widget_name/display", $args['display'] );
		}
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		if ( is_array( $this->fields ) && count( $this->fields ) ) {
			foreach ( $this->fields as $id => $param ) {
				switch ( $param['type'] ) {
					case 'custom':
						echo wp_kses_post( $param['content'] );
						break;
					case 'text':
						printf(
							'<p><label for="%1$s">%2$s</label><input class="widefat" id="%1$s" name="%3$s" type="text" value="%4$s"></p>',
							esc_attr( $this->get_field_id( $id ) ),
							esc_html( $param['title'] ),
							esc_attr( $this->get_field_name( $id ) ),
							esc_attr( $instance[ $id ] )
						); // WPCS XSS ok
						break;
					case 'number':
						printf(
							'<p><label for="%1$s">%2$s</label><input class="widefat" id="%1$s" name="%3$s" type="number" min="%5$s" max="%6$s" value="%4$s"/></p>',
							esc_attr( $this->get_field_id( $id ) ),
							esc_html( $param['title'] ),
							esc_attr( $this->get_field_name( $id ) ),
							esc_attr( $instance[ $id ] ),
							esc_attr( $param['options']['min'] ),
							esc_attr( $param['options']['max'] )
						); // WPCS XSS ok
						break;
					case 'dropdown':
						$all_options = '';

						foreach ( $param['options'] as $value => $name ) {
							$selected = $instance[ $id ] === $value ? 'selected="selected"' : '';

							$all_options .= "<option $selected value='$value'>$name</option>";
						}

						printf(
							'<p><label for="%1$s">%2$s</label><select class="widefat" id="%1$s" name="%3$s">%4$s</select></p>',
							esc_attr( $this->get_field_id( $id ) ),
							esc_html( $param['title'] ),
							esc_attr( $this->get_field_name( $id ) ),
							$all_options
						);// WPCS XSS ok
						break;
					case 'dropdown_with_status':
						$all_options = '';

						foreach ( $param['options'] as $value => $setting ) {
							$selected = $instance[ $id ] === $value ? 'selected="selected"' : '';

							$name   = $setting['name'];
							$status = $setting['enable'] ? null : 'disabled';

							$all_options .= "<option $status $selected value='$value'>$name</option>";
						}

						printf(
							'<p><label for="%1$s">%2$s</label><select class="widefat" id="%1$s" name="%3$s">%4$s</select></p>',
							esc_attr( $this->get_field_id( $id ) ),
							esc_html( $param['title'] ),
							esc_attr( $this->get_field_name( $id ) ),
							$all_options
						);// WPCS XSS ok
						break;
					case 'dropdown_all_users':
						$all_user = '';
						$users    = get_users();
						foreach ( $users as $user ) {
							$selected = (int) $instance[ $id ] === $user->ID ? 'selected="selected"' : '';

							$all_user .= "<option $selected value='$user->ID'>$user->display_name</option>";
						}

						printf(
							'<p><label for="%1$s">%2$s</label><select class="widefat" id="%1$s" name="%3$s">%4$s</select></p>',
							esc_attr( $this->get_field_id( $id ) ),
							esc_html( $param['title'] ),
							esc_attr( $this->get_field_name( $id ) ),
							$all_user
						);// WPCS XSS ok
						break;
					case 'dropdown_all_menus':
						$total_menu = '';
						$menus      = get_terms( 'nav_menu', [ 'hide_empty' => false ] );

						if ( ! $menus ) {
							return new \WP_Error( 'no_menu', sprintf( wp_kses_post( __( 'No menus have been created yet. <a href="%s">Create some</a>.', 'vnh' ) ), esc_url( admin_url( 'nav-menus.php' ) ) ) );
						}

						foreach ( $menus as $menu ) {
							$selected = $instance['nav_menu'] === $menu->slug ? ' selected="selected"' : '';

							$total_menu .= "<option $selected value='$menu->slug'>$menu->name</option>";
						}
						printf(
							'<p><label for="%1$s">%2$s</label><select class="widefat" id="%1$s" name="%3$s">%4$s</select></p>',
							esc_attr( $this->get_field_id( $id ) ),
							esc_html__( 'Select Menu:', 'vnh' ),
							esc_attr( $this->get_field_name( $id ) ),
							$total_menu
						); // WPCS XSS ok
				}
			}
		} else {
			esc_html_e( 'There are no options for this widget.', 'vnh' );
		}
	}

	protected function before_widget_content( $args, $instance ) {
		echo $args['before_widget']; // WPCS XSS ok

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title']; // WPCS XSS ok
		}
	}

	protected function after_widget_content( $args ) {
		echo $args['after_widget']; // WPCS XSS ok
	}

	protected static function create_widget_id( $input ) {
		$output = Helper::convert_to_snake_string( $input );
		$output = str_replace( 'framework_widgets_', '', $output );

		return $output;
	}

}
