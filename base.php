<?php

namespace VNH\Framework;

use VNH\Framework\Demo\Export;
use VNH\Framework\Demo\Import;
use VNH\Framework\Improvement\Disable_Attachment_Pages;
use VNH\Framework\Improvement\Purge_Transients;
use VNH\Framework\Improvement\Security;
use VNH\Framework\Improvement\SVG_Support;
use VNH\Framework\Improvement\Wrapper;
use VNH\Framework\Kirki\Kirki_Config;
use VNH\Framework\Kirki\Kirki_Wrapper;
use VNH\Framework\Like\Like;
use VNH\Framework\Shortcodes\Shortcodes;
use VNH\Framework\Social\Social_Menu;
use VNH\Framework\Social\Social_Share;

abstract class Base {
	public static $info;

	public static $breadcrumb_args, $tgmpa_register_plugins, $tgmpa_config, $social_networks, $ocdi_settings, $import_files, $menus, $icons, $extras, $security, $cpt_register, $ctax_register;

	public static $image_sizes, $default_image_sizes;

	public static $theme, $google_fonts;

	public static $customize;

	public static $modules, $default_modules;

	public static $classes, $default_classes;

	public static $transient_name, $cached;

	const REQUIRE_VERSION = '4.7';

	const PREFIX = 'vnh';

	const FRAMEWORK_DIR  = self::PREFIX . '/framework/';
	const SHORTCODES_DIR = self::FRAMEWORK_DIR . 'shortcodes/';
	const WIDGETS_DIR    = self::FRAMEWORK_DIR . 'widgets/';
	const MODULES_DIR    = self::FRAMEWORK_DIR . 'modules/';
	const ASSETS_DIR     = self::FRAMEWORK_DIR . 'assets/';

	const KIRKI_DIR = self::MODULES_DIR . 'kirki';
	const ACF_DIR   = self::MODULES_DIR . 'acf/';
	const TGMPA_DIR = self::MODULES_DIR . 'tgm_plugin_activation/';
	const OCDI_DIR  = self::MODULES_DIR . 'one-click-demo-import/';

	const SAMPLE_DIR = self::PREFIX . '/sample/';

	const SHORT_EXPIRATION = HOUR_IN_SECONDS;

	public function __construct() {
		$this->get_theme_info();
		$this->setup_vars();
		$this->set_transients();
		$this->delete_transients();
		$this->setup_ocdi_path_and_url();
		$this->init_framework_classes();
		$this->require_modules();
		$this->check_theme_update();
	}

	public function setup_vars() {
		self::$default_image_sizes = [
			'popular_widget' => [
				'desktop' => false,
				'tablet'  => false,
				'mobile'  => [
					'width'  => 300,
					'height' => 200,
				],
			],
		];
		self::$image_sizes         = apply_filters( 'vnh/f/base/image_sizes', self::$image_sizes );
		self::$image_sizes         = wp_parse_args( (array) self::$image_sizes, self::$default_image_sizes );

		self::$theme = [
			'main_js_handle'      => 'theme',
			'google_fonts_handle' => 'gg-fonts',
			'google_api_key'      => 'AIzaSyCwAq_RDWPBbKWITpsF1TH5V3tRgrrNX9w',
			'check_update_period' => 12,
		];
		self::$theme = apply_filters( 'vnh/f/base/theme', self::$theme );

		self::$google_fonts = apply_filters( 'vnh/f/base/google_fonts', self::$google_fonts );

		self::$customize = [
			'output_kirki_css_to_file' => true,
			'live_edit_title_desc'     => true,
		];

		self::$default_modules = [
			'acf'                   => true,
			'plugin-update-checker' => true,
			'otf_regen_thumbs'      => true,
			'one-click-demo-import' => true,
			'kirki'                 => true,
		];
		self::$modules         = apply_filters( 'vnh/f/base/modules', self::$modules );
		self::$modules         = wp_parse_args( (array) self::$modules, self::$default_modules );

		self::$default_classes = [
			'svg_support'  => true,
			'icons'        => true,
			'security'     => true,
			'social_menu'  => true,
			'like'         => true,
			'social_share' => true,
		];
		self::$classes         = apply_filters( 'vnh/f/base/classes', self::$classes );
		self::$classes         = wp_parse_args( (array) self::$classes, self::$default_classes );

		self::$transient_name = [
			'everything' => 'vnh_everything',
			'autoload'   => 'vnh_class_paths',
			'enqueue'    => 'vnh_enqueue',
			'icons'      => 'vnh_icons',
		];
		self::$cached         = [
			'siteurl'                => get_option( 'siteurl' ),
			'theme_slug'             => get_option( 'template' ),
			'child_theme_slug'       => get_option( 'stylesheet' ),
			'theme_version'          => self::$info['version'],
			'theme_name'             => self::$info['name'],
			'theme_author'           => self::$info['author'],
			'theme_author_uri'       => self::$info['author_uri'],
			'text_domain'            => self::$info['text_domain'],
			'child_text_domain'      => self::$info['child_text_domain'],
			'domain_directory'       => self::$info['parent_directory'] . self::$info['domain_path'],
			'child_domain_directory' => self::$info['child_directory'] . self::$info['child_domain_path'],
			'parent_directory'       => self::$info['parent_directory'],
			'parent_directory_uri'   => self::$info['parent_directory_uri'],
			'child_directory'        => self::$info['child_directory'],
			'child_directory_uri'    => self::$info['child_directory_uri'],
			'acf'                    => [
				'path' => self::$info['parent_directory'] . self::ACF_DIR,
				'dir'  => self::$info['parent_directory_uri'] . self::ACF_DIR,
			],
			'kirki'                  => [
				'url_path'       => self::$info['parent_directory_uri'] . self::KIRKI_DIR,
				'custom_css_url' => self::$info['parent_directory_uri'] . self::ASSETS_DIR . 'css/custom.css',
			],
			'widgets'                => [
				'mailchimp_admin_js_url' => self::$info['parent_directory_uri'] . self::WIDGETS_DIR . 'mailchimp/mailchimp_admin.js',
				'live_title_desc_js_url' => self::$info['parent_directory_uri'] . self::ASSETS_DIR . 'js/live_title_desc.js',
			],
			'twitter_oauth_path'     => self::$info['parent_directory'] . self::SHORTCODES_DIR . 'twitteroauth/autoload.php',
			'svg_icons_path'         => self::$info['parent_directory'] . 'assets/images/sprite.symbol.svg',
		];
	}

	public function set_transients() {
		$cached = get_transient( self::$transient_name['everything'] );

		if ( ! empty( $cached ) ) {
			self::$cached = $cached;
		}

		set_transient( self::$transient_name['everything'], self::$cached );

		return self::$cached;
	}

	protected function delete_transients() {
		// Delete theme transients if theme is changed or site url is changed or theme up to new version.
		if ( self::$cached['child_theme_slug'] !== get_option( 'stylesheet' ) || self::$cached['siteurl'] !== get_option( 'siteurl' ) || self::$cached['theme_version'] !== self::$info['version'] ) {
			foreach ( self::$transient_name as $name => $value ) {
				delete_transient( $value );
			}
		}
	}

	public function setup_ocdi_path_and_url() {
		define( 'PT_OCDI_PATH', self::$cached['parent_directory'] . self::OCDI_DIR );
		define( 'PT_OCDI_URL', self::$cached['parent_directory_uri'] . self::OCDI_DIR );
	}

	public function init_framework_classes() {
		new Purge_Transients();
		new Compatibility();
		new Wrapper(); // A Smarter Way to Load Header and Footer Elements
		new I18n();
		new Shortcodes();
		new Kirki_Config();
		new Kirki_Wrapper(); // This is a wrapper class for Kirki
		new Menus();
		new Export();
		new ACF();
		new Breadcrumb();
		new Disable_Attachment_Pages();
		if ( ! empty( self::$cpt_register ) ) {
			new Cpt();
		}
		if ( ! empty( self::$ctax_register ) ) {
			new Ctax();
		}
		if ( ! empty( self::$modules['one-click-demo-import'] ) ) {
			new Import();
		}
		if ( ! empty( self::$classes['svg_support'] ) ) {
			new SVG_Support();
		}
		if ( ! empty( self::$classes['icons'] ) ) {
			new Icons();
		}
		if ( ! empty( self::$classes['security'] ) ) {
			new Security();
		}
		if ( ! empty( self::$classes['social_menu'] ) ) {
			new Social_Menu();
		}
		if ( ! empty( self::$classes['like'] ) ) {
			new Like();
		}
		if ( ! empty( self::$classes['social_share'] ) ) {
			new Social_Share();
		}
	}

	public function require_modules() {
		foreach ( self::$modules as $module => $status ) {
			if ( ! empty( $status ) ) {
				require_once self::$cached['parent_directory'] . self::MODULES_DIR . $module . '/' . $module . '.php';
			}
		}

		if ( ! empty( self::$tgmpa_register_plugins ) ) {
			require_once self::$cached['parent_directory'] . self::TGMPA_DIR . 'tgm_plugin_activation.php';
		}
	}

	public function check_theme_update() {
		if ( empty( self::$modules['plugin-update-checker'] ) ) {
			return;
		}
		$metadata_url    = self::$cached['theme_author_uri'] . 'update/?action=get_metadata&slug=' . self::$cached['theme_slug'];
		$main_theme_file = self::$cached['parent_directory'] . 'functions.php';
		\Puc_v4p3_Factory::buildUpdateChecker( $metadata_url, $main_theme_file, self::$cached['theme_slug'], self::$theme['check_update_period'] );
	}

	private function get_theme_info() {
		$parent_theme = wp_get_theme( get_template() );
		$child_theme  = wp_get_theme();
		self::$info   = [
			'name'                 => $parent_theme->get( 'Name' ),
			'version'              => $parent_theme->get( 'Version' ),
			'author'               => $parent_theme->get( 'Author' ),
			'theme_uri'            => trailingslashit( $parent_theme->get( 'ThemeURI' ) ),
			'author_uri'           => trailingslashit( $parent_theme->get( 'AuthorURI' ) ),
			'text_domain'          => sanitize_key( $parent_theme->get( 'TextDomain' ) ),
			'child_text_domain'    => sanitize_key( $child_theme->get( 'TextDomain' ) ),
			'domain_path'          => trim( $parent_theme->get( 'DomainPath' ), '/' ) ? : 'languages',
			'child_domain_path'    => trim( $child_theme->get( 'DomainPath' ), '/' ) ? : 'languages',
			'parent_directory'     => trailingslashit( get_template_directory() ),
			'parent_directory_uri' => trailingslashit( get_template_directory_uri() ),
			'child_directory'      => trailingslashit( get_stylesheet_directory() ),
			'child_directory_uri'  => trailingslashit( get_stylesheet_directory_uri() ),
		];

		return self::$info;
	}
}
