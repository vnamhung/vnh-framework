<?php

namespace VNH\Framework;

class Image {
	public $args, $full_img_url, $full_width, $full_height;

	public function __construct( $args, $echo = true ) {
		$defaults   = [
			'img_ID'       => get_post_thumbnail_id( get_the_ID() ),
			'desktop'      => [
				'width'  => 0,
				'height' => 0,
			],
			'tablet'       => [
				'width'  => 0,
				'height' => 0,
			],
			'mobile'       => [
				'width'  => 0,
				'height' => 0,
			],
			'ratio'        => 0.1,
			'custom_class' => '',
			'placeholder'  => true,
			'before'       => '',
			'after'        => '',
		];
		$this->args = wp_parse_args( (array) $args, $defaults );

		$this->full_img_url = wp_get_attachment_image_url( $this->args['img_ID'], 'full' );

		if ( empty( $this->full_img_url ) ) {
			return null;
		}

		list( $this->full_width, $this->full_height ) = getimagesize( $this->full_img_url );

		if ( $echo ) {
			echo $this->get_picture(); // WPCS XSS ok
		}
	}

	public function __toString() {
		return (string) $this->get_picture();
	}

	public function get_picture( $picture = '' ) {
		$custom_class = $this->args['custom_class'];
		$placeholder  = $this->get_placeholder();
		$size         = $this->get_size();
		$alt          = get_post_meta( $this->args['img_ID'], '_wp_attachment_image_alt', true );

		$picture .= $this->args['before'];
		$picture .= '<picture class="lazy-wrapper">';
		$picture .= $this->get_desktop_img();
		$picture .= $this->get_tablet_img();
		$picture .= $this->get_mobile_img();
		$picture .= "<img class='lazy $custom_class' src='$placeholder' alt='$alt' $size />";
		$picture .= '</picture>';
		$picture .= $this->args['after'];

		return $picture;
	}

	public function get_desktop_img() {
		if ( empty( $this->args['desktop'] ) ) {
			return null;
		}

		return $this->get_img( 'desktop', 1200 );
	}

	public function get_tablet_img() {
		if ( empty( $this->args['tablet'] ) ) {
			return null;
		}

		return $this->get_img( 'tablet', 768 );
	}


	public function get_mobile_img() {
		if ( empty( $this->args['mobile'] ) ) {
			return null;
		}

		return $this->get_img( 'mobile', 375 );
	}

	public function get_img( $type, $media_min_width, $unit = 'px' ) {
		$crop = $this->args[ $type ];

		if ( empty( $crop['width'] ) && empty( $crop['height'] ) || ( $this->full_width === $crop['width'] && $this->full_height === $crop['height'] ) ) {
			$normal_img_url = $this->full_img_url;
			$webp_img_url   = $this->get_webp_img( $normal_img_url );
		} else {
			$normal_img_url = wp_get_attachment_image_url( $this->args['img_ID'], [ $crop['width'], $crop['height'] ] );
			$webp_img_url   = $this->get_webp_img( $normal_img_url );

			if ( $this->full_width > $crop['width'] * 2 && $this->full_height > $crop['height'] * 2 ) {
				$retina_img_url = wp_get_attachment_image_url( $this->args['img_ID'], [
					$crop['width'] * 2,
					$crop['height'] * 2,
				] );
			} elseif ( $this->full_width === $crop['width'] * 2 ) {
				$retina_img_url = $this->full_img_url;
			} else {
				$retina_img_url = null;
			}
		}

		$retina_img_url = ! empty( $retina_img_url ) ? ", $retina_img_url 2x" : '';

		$source = '';

		if ( ! empty( $webp_img_url ) ) {
			$source = "<source data-srcset='$webp_img_url 1x $retina_img_url' media='(min-width: $media_min_width$unit)' type='image/webp'>";
		}

		$source .= "<source data-srcset='$normal_img_url 1x $retina_img_url' media='(min-width: $media_min_width$unit)'>";

		return $source; // WPCS XSS ok
	}

	public function get_size() {
		if ( ! empty( $this->args['desktop']['width'] ) ) {
			$crop = $this->args['desktop'];
		} elseif ( ! empty( $this->args['tablet']['width'] ) ) {
			$crop = $this->args['tablet'];
		} else {
			$crop = $this->args['mobile'];
		}

		if ( empty( $crop['width'] ) || empty( $crop['height'] ) ) {
			$width  = $this->full_width;
			$height = $this->full_height;
		} else {
			$width  = $crop['width'];
			$height = $crop['height'];
		}

		return "width='$width' height='$height'";
	}

	public function get_placeholder() {
		if ( empty( $this->args['placeholder'] ) ) {
			return null;
		}

		if ( ! empty( $this->get_svg_placeholder() ) ) {
			$placeholder = $this->get_svg_placeholder();
		} else {
			$crop  = $this->args['desktop'];
			$ratio = $this->args['ratio'];

			if ( empty( $crop['width'] ) || empty( $crop['height'] ) ) {
				$img_placeholder_size = [ $this->full_width * $ratio, $this->full_height * $ratio ];
			} else {
				$img_placeholder_size = [ $crop['width'] * $ratio, $crop['height'] * $ratio ];
			}
			$placeholder = wp_get_attachment_image_url( $this->args['img_ID'], $img_placeholder_size );
		}

		return $placeholder;
	}

	public function get_svg_placeholder() {
		$uploads_dir          = wp_upload_dir();
		$svg_placeholder_url  = preg_replace( '/\.(jpe?g|png|gif|bmp)$/i', '.svg', $this->full_img_url );
		$svg_placeholder_path = str_replace( $uploads_dir['baseurl'], $uploads_dir['basedir'], $svg_placeholder_url );

		if ( ! file_exists( $svg_placeholder_path ) ) {
			return null;
		}

		$svg_content     = Helper::get_file_content( $svg_placeholder_url );
		$svg_content     = rawurlencode( $svg_content );
		$svg_placeholder = 'data:image/svg+xml;utf8,' . $svg_content;

		return $svg_placeholder;
	}

	public function get_webp_img( $img_url ) {
		$uploads_dir   = wp_upload_dir();
		$webp_img_url  = preg_replace( '/\.(jpe?g|png|gif|bmp)$/i', '.webp', $img_url );
		$webp_img_path = str_replace( $uploads_dir['baseurl'], $uploads_dir['basedir'], $webp_img_url );

		if ( ! file_exists( $webp_img_path ) ) {
			return null;
		}

		return $webp_img_url;
	}
}
