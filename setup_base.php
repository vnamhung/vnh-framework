<?php

namespace VNH\Framework;

abstract class Setup_Base {
	public $content_width = 640;
	public $post_formats  = [ 'video', 'quote', 'link', 'gallery', 'audio' ];
	public $custom_logo;
	public $custom_background;

	public $tgmpa_config, $tgmpa_register_plugins;

	public function __construct() {
		$this->apply_filters();

		add_action( 'after_setup_theme', [ $this, 'content_width' ], 0 );
		add_action( 'tgmpa_register', [ $this, 'register_plugins' ] );
		add_action( 'after_setup_theme', [ $this, 'theme_support' ] );
		add_action( 'after_setup_theme', [ $this, 'excerpt' ] );

		if ( class_exists( 'WooCommerce' ) ) {
			$this->woocommerce_support();
		}
	}

	public function apply_filters() {
		$this->content_width     = apply_filters( 'vnh/f/setup/content_width', $this->content_width );
		$this->post_formats      = apply_filters( 'vnh/f/setup/post_formats', $this->post_formats );
		$this->custom_logo       = apply_filters( 'vnh/f/setup/custom_logo', $this->custom_logo );
		$this->custom_background = apply_filters( 'vnh/f/setup/custom_background', $this->custom_background );
	}

	public function content_width() {
		$GLOBALS['content_width'] = $this->content_width;
	}

	public function register_plugins() {
		$default_config     = [
			'id'           => Base::PREFIX,                              // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                                        // Default absolute path to bundled plugins.
			'menu'         => Base::PREFIX . '-install-plugins',         // Menu slug.
			'has_notices'  => true,                                      // Show admin notices or not.
			'dismissable'  => true,                                      // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => '',                                        // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => true,                                      // Automatically activate plugins after installation or not.
			'message'      => '',                                        // Message to output right before the plugins table.
		];
		$this->tgmpa_config = apply_filters( 'vnh/f/setup/tgmpa/config', Base::$tgmpa_config );
		$this->tgmpa_config = wp_parse_args( (array) $this->tgmpa_config, $default_config );

		$this->tgmpa_register_plugins = apply_filters( 'vnh/f/setup/tgmpa/register_plugins', Base::$tgmpa_register_plugins );

		return tgmpa( $this->tgmpa_register_plugins, $this->tgmpa_config );
	}

	public function theme_support() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'html5', [ 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ] );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'post-formats', $this->post_formats );
		add_theme_support( 'post-thumbnails' );
	}

	public function excerpt() {
		add_filter( 'the_excerpt', 'shortcode_unautop' ); // Don't autop in excerpt.
		add_filter( 'the_excerpt', 'do_shortcode' ); // Allow the Excerpt to handle shortcodes.
	}

	public function woocommerce_support() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}
}
