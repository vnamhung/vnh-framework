<?php

namespace VNH\Framework;

class Icons {
	public $svg_path;

	public function __construct() {
		$this->svg_path = Base::$cached['svg_icons_path'];
		$this->svg_path = apply_filters( 'vnh/f/icons/svg_path', $this->svg_path );

		add_action( 'vnh/h/before/page', [ $this, 'include_svg_icons' ] );
	}

	public function include_svg_icons() {
		$cached_svg_content = is_dev() ? null : get_transient( Base::$transient_name['icons'] );

		if ( ! empty( $cached_svg_content ) ) {
			echo $cached_svg_content; // WPCS XSS ok

			return;
		}

		$svg_content = Helper::get_file_content( $this->svg_path );
		$svg_content = str_replace( '<?xml version="1.0" encoding="utf-8"?>', '', $svg_content );

		set_transient( Base::$transient_name['icons'], $svg_content );
		echo $svg_content; // WPCS XSS ok
	}
}
