<?php
/**
 * Social Menu Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Social;

use VNH\Framework\Widgets\Widget;

class Social_Widget extends Widget {
	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Social', 'vnh' ),
				'classname'   => 'widget-social',
				'description' => esc_html__( 'Displays your social network links.', 'vnh' ),
				'fields'      => [
					'title' => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Social Network Links', 'vnh' ),
					],
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		do_action( 'vnh/h/social_menu' );

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );

		return $instance;
	}
}
