<?php
/**
 * Recent Posts Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

class Recent_Posts extends Widget {
	public $display;

	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Recent Posts', 'vnh' ),
				'classname'   => 'widget-recent-posts',
				'description' => esc_html__( 'Display recent posts with thumbnail.', 'vnh' ),
				'fields'      => [
					'title'  => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Recent Posts', 'vnh' ),
					],
					'number' => [
						'title'   => __( 'Number of posts:', 'vnh' ),
						'type'    => 'number',
						'options' => [
							'min' => 1,
							'max' => 10,
						],
						'default' => 4,
					],
				],
				'display'     => [
					'before'           => '<ul class="recent-posts__items">',
					'after'            => '</ul>',
					'content'          => '<li class="recent-posts__item"><img src="%1$s" alt="" /><a href="%2$s">%3$s</a><span class="meta-data">%4$s | %5$s</span></li>',
					'thumbnail_width'  => 50,
					'thumbnail_height' => 50,
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		do_shortcode(
			sprintf(
				'[recent_posts number = % before=\'%s\' after=\'%s\' content=\'%s\' thumbnail_width = \'%s\' thumbnail_height = \'%s\' ]',
				$instance['number'],
				$this->display['before'],
				$this->display['after'],
				$this->display['content'],
				$this->display['thumbnail_width'],
				$this->display['thumbnail_height']
			)
		);

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = ( (int) $new_instance['number'] !== 0 ) ? (int) $new_instance['number'] : null;

		return $instance;
	}
}
