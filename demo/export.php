<?php

namespace VNH\Framework\Demo;

class Export {
	public function __construct() {
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			\WP_CLI::add_command( 'vnh export menus', [ $this, 'export_menus' ] );
			\WP_CLI::add_command( 'vnh export homepage_settings', [ $this, 'export_homepage_settings' ] );
			\WP_CLI::add_command( 'vnh export widgets', [ $this, 'export_widgets' ] );
			\WP_CLI::add_command( 'vnh export customizer', [ $this, 'export_customizer_options' ] );
		}
	}

	public function export_widgets() {
		// Get all available widgets site
		$available_widgets = $this->available_widgets();

		// Get all widget instances for each widget
		$widget_instances = [];
		foreach ( $available_widgets as $widget_data ) {

			// Get all instances for this ID base
			$instances = get_option( 'widget_' . $widget_data['id_base'] );

			// Have instances
			if ( ! empty( $instances ) ) {
				// Loop instances
				foreach ( $instances as $instance_id => $instance_data ) {
					// Key is ID (not _multiwidget)
					if ( is_numeric( $instance_id ) ) {
						$unique_instance_id                      = $widget_data['id_base'] . '-' . $instance_id;
						$widget_instances[ $unique_instance_id ] = $instance_data;
					}
				}
			}
		}

		// Gather sidebars with their widget instances
		$sidebars_widgets          = get_option( 'sidebars_widgets' ); // get sidebars and their unique widgets IDs
		$sidebars_widget_instances = [];

		foreach ( $sidebars_widgets as $sidebar_id => $widget_ids ) {
			// Skip inactive widgets
			if ( $sidebar_id === 'wp_inactive_widgets' ) {
				continue;
			}

			// Skip if no data or not an array (array_version)
			if ( ! is_array( $widget_ids ) || empty( $widget_ids ) ) {
				continue;
			}

			// Loop widget IDs for this sidebar
			foreach ( $widget_ids as $widget_id ) {
				// Is there an instance for this widget ID?
				if ( isset( $widget_instances[ $widget_id ] ) ) {
					// Add to array
					$sidebars_widget_instances[ $sidebar_id ][ $widget_id ] = $widget_instances[ $widget_id ];
				}
			}
		}

		$data = wp_json_encode( $sidebars_widget_instances );

		echo $data; //WPCS XSS ok
	}

	public function export_menus() {
		global $wpdb;

		$data        = [];
		$locations   = get_nav_menu_locations();
		$terms_table = $wpdb->prefix . 'terms';

		foreach ( (array) $locations as $location => $menu_id ) {
			$menu_slug = $wpdb->get_results( "SELECT * FROM $terms_table where term_id={$menu_id}", ARRAY_A ); //phpcs:disable

			if ( ! empty( $menu_slug ) ) {
				$data[ $location ] = $menu_slug[0]['slug'];
			}
		}

		$output = wp_json_encode( $data );

		echo $output; //WPCS XSS ok
	}

	public function export_homepage_settings() {
		$show_on_front  = get_option( 'show_on_front' );
		$post_page_id   = get_option( 'page_for_posts' );
		$static_page_id = get_option( 'page_on_front' );
		$settings_pages = [
			'show_on_front' => $show_on_front,
		];

		if ( $static_page_id ) {
			$settings_pages['page_on_front'] = get_post( $static_page_id )->post_title;
		}

		if ( $post_page_id ) {
			$settings_pages['page_for_posts'] = get_post( $post_page_id )->post_title;
		}

		$output = wp_json_encode( $settings_pages );

		echo $output; //WPCS XSS ok
	}

	public function export_customizer_options() {
		$options = get_theme_mods();
		unset( $options['nav_menu_locations'] );

		$output = wp_json_encode( $options );

		echo $output; //WPCS XSS ok
	}

	private function available_widgets() {
		global $wp_registered_widget_controls;

		$widget_controls   = $wp_registered_widget_controls;
		$available_widgets = [];

		foreach ( $widget_controls as $widget ) {
			if ( ! empty( $widget['id_base'] ) && ! isset( $available_widgets[ $widget['id_base'] ] ) ) { // no dupes
				$available_widgets[ $widget['id_base'] ]['id_base'] = $widget['id_base'];
				$available_widgets[ $widget['id_base'] ]['name']    = $widget['name'];
			};
		}

		return $available_widgets;
	}
}
