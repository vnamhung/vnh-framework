<?php

namespace VNH\Framework;

class Ctax {
	public $ctax_register;

	public function __construct() {
		$this->ctax_register = apply_filters( 'vnh/f/ctax/register', Base::$ctax_register );

		add_action( 'init', [ $this, 'register_taxonomies' ], 0 );
	}

	public function register_taxonomies() {
		foreach ( $this->ctax_register as $tax_key => $args ) {
			$this->register_taxonomy( $tax_key, $args );
		}
	}

	protected function register_taxonomy( $tax_key, $args = [] ) {
		$default = [
			'labels'            => [
				'all_items'                  => esc_html__( 'All Items', 'vnh' ),
				'parent_item'                => esc_html__( 'Parent Item', 'vnh' ),
				'parent_item_colon'          => esc_html__( 'Parent Item:', 'vnh' ),
				'new_item_name'              => esc_html__( 'New Item Name', 'vnh' ),
				'add_new_item'               => esc_html__( 'Add New Item', 'vnh' ),
				'edit_item'                  => esc_html__( 'Edit Item', 'vnh' ),
				'update_item'                => esc_html__( 'Update Item', 'vnh' ),
				'view_item'                  => esc_html__( 'View Item', 'vnh' ),
				'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'vnh' ),
				'add_or_remove_items'        => esc_html__( 'Add or remove items', 'vnh' ),
				'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'vnh' ),
				'popular_items'              => esc_html__( 'Popular Items', 'vnh' ),
				'search_items'               => esc_html__( 'Search Items', 'vnh' ),
				'not_found'                  => esc_html__( 'Not Found', 'vnh' ),
				'no_terms'                   => esc_html__( 'No items', 'vnh' ),
				'items_list'                 => esc_html__( 'Items list', 'vnh' ),
				'items_list_navigation'      => esc_html__( 'Items list navigation', 'vnh' ),
			],
			'hierarchical'      => false,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
		];

		$args           = wp_parse_args( (array) $args, $default );
		$args['labels'] = wp_parse_args( (array) $args['labels'], $default['labels'] );

		register_taxonomy( $tax_key, $args['post_type'], $args );
	}
}
