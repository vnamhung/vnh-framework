<?php
/**
 * Twitter Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

class Twitter extends Widget {
	public $display;

	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Twitter', 'vnh' ),
				'classname'   => 'widget-twitter',
				'description' => esc_html__( 'Displays your latest tweets.', 'vnh' ),
				'fields'      => [
					'custom'              => [
						'type'    => 'custom',
						'content' => __( '<p>Get your API keys &amp; tokens at:<br /><a href="https://apps.twitter.com/" target="_blank">https://apps.twitter.com/</a></p>', 'vnh' ),
					],
					'title'               => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Twitter', 'vnh' ),
					],
					'consumer_key'        => [
						'title'   => __( 'Consumer Key:', 'vnh' ),
						'type'    => 'text',
						'default' => '',
					],
					'consumer_secret'     => [
						'title'   => __( 'Consumer Secret:', 'vnh' ),
						'type'    => 'text',
						'default' => '',
					],
					'access_token'        => [
						'title'   => __( 'Access Token:', 'vnh' ),
						'type'    => 'text',
						'default' => '',
					],
					'access_token_secret' => [
						'title'   => __( 'Access Token Secret:', 'vnh' ),
						'type'    => 'text',
						'default' => '',
					],
					'username'            => [
						'title'   => __( 'Twitter Username:', 'vnh' ),
						'type'    => 'text',
						'default' => '',
					],
					'tweets_to_show'      => [
						'title'   => __( 'Tweets to show:', 'vnh' ),
						'type'    => 'number',
						'options' => [
							'min' => 1,
							'max' => 10,
						],
						'default' => 1,
					],
				],
				'display'     => [
					'before'  => '<ul class="twitter__tweets">',
					'after'   => '</ul>',
					'content' => '<li class="twitter__tweet"><span>%1$s</span><a class="twitter__permalink" href="%2$s" target="_blank" rel="nofollow"><time class="twitter__time" datetime="%3$s">%4$s</time></a></li>',
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		//check settings and die if not set
		if ( empty( $instance['consumer_key'] ) || empty( $instance['consumer_secret'] ) || empty( $instance['access_token'] ) || empty( $instance['access_token_secret'] ) || empty( $instance['username'] ) ) {
			return new \WP_Error( 'setting_error', esc_html__( 'Please fill all widget settings!', 'vnh' ) );
		}

		do_shortcode(
			sprintf(
				'[twitter consumer_key = %s consumer_secret = %s access_token = %s access_token_secret = %s username = %s tweets_to_show = %s transient_name = %s before=\'%s\' after=\'%s\' content=\'%s\' ]',
				$instance['consumer_key'],
				$instance['consumer_secret'],
				$instance['access_token'],
				$instance['access_token_secret'],
				$instance['username'],
				$instance['tweets_to_show'],
				$this->id,
				$this->display['before'],
				$this->display['after'],
				$this->display['content']
			)
		);

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance                        = $old_instance;
		$instance['title']               = sanitize_text_field( $new_instance['title'] );
		$instance['consumer_key']        = sanitize_text_field( $new_instance['consumer_key'] );
		$instance['consumer_secret']     = sanitize_text_field( $new_instance['consumer_secret'] );
		$instance['access_token']        = sanitize_text_field( $new_instance['access_token'] );
		$instance['access_token_secret'] = sanitize_text_field( $new_instance['access_token_secret'] );
		$instance['username']            = sanitize_text_field( $new_instance['username'] );
		$instance['tweets_to_show']      = ( 0 !== (int) $new_instance['tweets_to_show'] ) ? (int) $new_instance['tweets_to_show'] : null;

		if (
			$old_instance['consumer_key'] !== $new_instance['consumer_key'] ||
			$old_instance['consumer_secret'] !== $new_instance['consumer_secret'] ||
			$old_instance['access_token'] !== $new_instance['access_token'] ||
			$old_instance['access_token_secret'] !== $new_instance['access_token_secret'] ||
			$old_instance['username'] !== $new_instance['username'] ||
			$old_instance['tweets_to_show'] !== $new_instance['tweets_to_show']
		) {
			delete_transient( $this->id );
		}

		return $instance;
	}
}
