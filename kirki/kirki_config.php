<?php

namespace VNH\Framework\Kirki;

use VNH\Framework\Base;

class Kirki_Config {
	public function __construct() {
		add_filter( 'kirki_config', [ $this, 'kirki_update_url' ] );
		add_action( 'widgets_init', [ $this, 'kirki_add_config' ], 99 );
		add_filter( 'kirki_dynamic_css_method', [ $this, 'output_kirki_css_to_file' ] );
		add_action( 'customize_controls_init', [ $this, 'additional_css_customizer' ] );
		add_filter( 'kirki_values_get_value', [ $this, 'kirki_db_get_theme_mod_value' ], 10, 2 );
	}

	public function kirki_update_url( $config ) {
		$config['url_path'] = Base::$cached['kirki']['url_path'];

		return $config;
	}

	public function kirki_add_config() {
		Kirki_Wrapper::add_config( Base::$cached['theme_slug'], [
			'option_type' => 'theme_mod',
			'capability'  => 'edit_theme_options',
		] );
	}

	public function output_kirki_css_to_file() {
		return empty( Base::$customize['output_kirki_css_to_file'] ) ? null : 'file';
	}

	public function additional_css_customizer() {
		wp_enqueue_style( 'kirki-custom-css', Base::$cached['kirki']['custom_css_url'] );
	}

	public function kirki_db_get_theme_mod_value( $value, $setting ) {
		static $settings;

		if ( is_null( $settings ) ) {
			$settings = [];

			if ( ! empty( $_GET ) ) { //phpcs:disable
				foreach ( $_GET as $key => $query_value ) { //phpcs:disable
					if ( ! empty( \Kirki::$fields[ $key ] ) ) {
						$settings[ $key ] = $query_value;

						if ( is_array( \Kirki::$fields[ $key ] ) && \Kirki::$fields[ $key ]['type'] === 'kirki-preset' && ! empty( \Kirki::$fields[ $key ]['choices'] ) && ! empty( \Kirki::$fields[ $key ]['choices'][ $query_value ] ) && ! empty( \Kirki::$fields[ $key ]['choices'][ $query_value ]['settings'] ) ) {
							foreach ( \Kirki::$fields[ $key ]['choices'][ $query_value ]['settings'] as $kirki_setting => $kirki_value ) {
								$settings[ $kirki_setting ] = $kirki_value;
							}
						}
					}
				}
			}
		}

		if ( isset( $settings[ $setting ] ) ) {
			return $settings[ $setting ];
		}

		return $value;
	}
}
