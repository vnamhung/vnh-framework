<?php
/**
 * Plugin Name: BE Like Content
 * Plugin URI:  https://github.com/billerickson/be-like-content
 * Description: Allow users to like content
 * Author:      Bill Erickson
 * Author URI:  https://www.billerickson.net
 * Version:     1.1.0
 *
 * BE Like Content is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * BE Like Content is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BE Like Content. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    BE_Like_Content
 * @author     Bill Erickson
 * @since      1.0.0
 * @license    GPL-2.0+
 * @copyright  Copyright (c) 2017
 */

namespace VNH\Framework\Like;

use VNH\Framework\Base;

class Like {
	public static $settings = [];
	public static $status   = false;

	public function __construct() {
		self::$settings = [
			'zero'       => esc_html__( 'Like this post', 'vnh' ),
			'single'     => esc_html__( 'like', 'vnh' ),
			'plural'     => esc_html__( 'likes', 'vnh' ),
		];
		self::$settings = apply_filters( 'vnh/f/like/settings', self::$settings );

		self::$status = true;

		add_action( 'wp_enqueue_scripts', [ $this, 'scripts' ] );
		add_action( 'wp_ajax_be_like_content', [ $this, 'update_count' ] );
		add_action( 'wp_ajax_nopriv_be_like_content', [ $this, 'update_count' ] );
		add_action( 'vnh/h/like', [ $this, 'display_likes' ] );

		// Dashboard Widget
		add_action( 'wp_dashboard_setup', [ $this, 'register_dashboard_widget' ] );
	}

	public function scripts() {
		wp_register_script( 'be_like_content', Base::$cached['parent_directory_uri'] . Base::FRAMEWORK_DIR . 'like/js/be-like-content.min.js', [ 'jquery' ], null, true );
		wp_localize_script( 'be_like_content', 'be_like_content', [ 'url' => admin_url( 'admin-ajax.php' ) ] );

		if ( is_single() ) {
			wp_enqueue_script( 'be_like_content' );
		}
	}

	public function update_count() {
		$post_id = intval( $_POST['post_id'] );

		if ( ! $post_id ) {
			wp_send_json_error( __( 'No Post ID', 'vnh' ) );
		}

		$count = $this->count( $post_id );
		$count++;
		update_post_meta( $post_id, '_be_like_content', $count );

		$data = $this->maybe_count( $post_id, $count );
		wp_send_json_success( $data );

		wp_die();
	}

	public function display_likes() {
		printf(
			'<a href="#" class="be-like-content" data-post-id="%1$s">%2$s</a>',
			esc_attr( get_the_ID() ),
			esc_html( $this->maybe_count( get_the_ID() ) )
		);
	}

	public function maybe_count( $post_id = '', $count = false ) {
		if ( empty( $post_id ) ) {
			return null;
		}

		$count = $count ? intval( $count ) : $this->count( $post_id );
		$text  = $count === 0 ? self::$settings['zero'] : sprintf( '%s %s', $count, _n( self::$settings['single'], self::$settings['plural'], $count ) ); //phpcs:disable

		return $text;
	}

	public function count( $post_id = '' ) {
		if ( empty( $post_id ) ) {
			return null;
		}

		return intval( get_post_meta( $post_id, '_be_like_content', true ) );
	}

	public function register_dashboard_widget() {
		wp_add_dashboard_widget(
			'like_popular_widget',
			__( 'Popular Liked Posts', 'vnh' ),
			[ $this, 'dashboard_widget', ]
		);
	}

	public function dashboard_widget() {
		$args = [
			'posts_per_page' => 5,
			'orderby'        => 'meta_value_num',
			'order'          => 'DESC',
			'meta_key'       => '_be_like_content',
		];
		$args = apply_filters( 'vnh/f/like/dashboard_args', $args );

		$loop = new \WP_Query( $args );

		if ( $loop->have_posts() ) :
			$html = '<ol>';

			while ( $loop->have_posts() ) :
				$loop->the_post();

				$likes = $this->count( get_the_ID() );

				$html .= sprintf(
					'<li><a href="%s">%s (%s %s)</a></li>',
					get_permalink(),
					get_the_title(),
					$likes,
					_n( self::$settings['single'], self::$settings['plural'], $likes )
				);

			endwhile;

			$html .= '</ol>';

			echo wp_kses_post( $html );

		endif;

		wp_reset_postdata();
	}
}
