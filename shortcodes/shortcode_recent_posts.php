<?php

namespace VNH\Framework\Shortcodes;

use VNH\Framework\Helper;

class Shortcode_Recent_Posts {
	public static $defaults;

	public function __construct() {
		self::$defaults = [
			'number'           => 5,
			'before'           => '<ul class="recent-posts__items">',
			'after'            => '</ul>',
			'content'          => '<li class="recent-posts__item"><img src="%1$s" alt="" /><a href="%2$s">%3$s</a><span class="meta-data">%4$s | %5$s</span></li>',
			'thumbnail_width'  => 50,
			'thumbnail_height' => 50,
		];
		self::$defaults = apply_filters( 'vnh/f/shortcode/recent_posts/defaults', self::$defaults );

		add_shortcode( 'recent_posts', [ $this, 'create_shortcode' ] );
	}

	public function create_shortcode( $atts ) {
		$atts   = shortcode_atts( self::$defaults, $atts );
		$width  = $atts['thumbnail_width'];
		$height = $atts['thumbnail_height'];
		$query  = new \WP_Query( [ 'posts_per_page' => $atts['number'] ] );

		if ( $query->have_posts() ) :
			$html = $atts['before'];

			while ( $query->have_posts() ) :
				$query->the_post();

				$img_url = get_the_post_thumbnail_url( null, [ $width, $height ] );

				if ( file_exists( get_theme_file_path( 'template-parts/shortcode/recent.php' ) ) ) {
					get_template_part( 'template-parts/shortcode/recent' );
				} else {
					$html .= sprintf( $atts['content'], $img_url, get_the_permalink(), get_the_title(), get_the_category_list(), Helper::human_time_diff_maybe( get_the_time( 'U' ), 7 ) );
				}
			endwhile;

			wp_reset_postdata();

			$html .= $atts['after'];

			echo $html; // WPCS XSS ok
		endif;
	}
}
