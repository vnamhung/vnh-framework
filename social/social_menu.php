<?php

namespace VNH\Framework\Social;

use VNH\Framework\Base;
use VNH\Framework\Helper;

class Social_Menu {
	protected static $default_icons = [
		'500px.com'       => '500px',
		'behance.net'     => 'behance',
		'codepen.io'      => 'codepen',
		'dribbble.com'    => 'dribbble',
		'dropbox.com'     => 'dropbox',
		'facebook.com'    => 'facebook',
		'flickr.com'      => 'flickr',
		'foursquare.com'  => 'foursquare',
		'plus.google.com' => 'google',
		'github.com'      => 'github',
		'instagram.com'   => 'instagram',
		'linkedin.com'    => 'linkedin',
		'mailto:'         => 'email',
		'medium.com'      => 'medium',
		'meetup.com'      => 'meetup',
		'pinterest.com'   => 'pinterest',
		'reddit.com'      => 'reddit',
		'smugmug.net'     => 'smugmug',
		'snapchat.com'    => 'snapchat-ghost',
		'soundcloud.com'  => 'soundcloud',
		'spotify.com'     => 'spotify',
		'stumbleupon.com' => 'stumbleupon',
		'tumblr.com'      => 'tumblr',
		'twitch.tv'       => 'twitch',
		'twitter.com'     => 'twitter',
		'vimeo.com'       => 'vimeo',
		'vine.co'         => 'vine',
		'wordpress.org'   => 'wordpress',
		'wordpress.com'   => 'wordpress',
		'yelp.com'        => 'yelp',
		'youtube.com'     => 'youtube',
	];

	private static $icons;

	public static $social_menu;

	public function __construct() {
		self::$icons = apply_filters( 'vnh/f/social_menu/icons', Base::$icons );
		self::$icons = wp_parse_args( (array) self::$icons, self::$default_icons );

		self::$social_menu = [
			'theme_location' => 'social',
			'menu_class'     => 'social-menu',
			'menu_desc'      => esc_html__( 'Social', 'vnh' ),
		];

		register_nav_menus( [ self::$social_menu['theme_location'] => self::$social_menu['menu_desc'] ] );

		register_widget( __NAMESPACE__ . '\Social_Widget' );

		add_filter( 'walker_nav_menu_start_el', [ $this, 'nav_menu_social_icons' ], 10, 4 ); // Change SVG icon inside social links menu if there is supported URL.

		add_action( 'vnh/h/social_menu', [ $this, 'the_menu' ] );

		add_shortcode( 'social_menu', [ $this, 'get_menu' ] );
	}

	public function nav_menu_social_icons( $item_output, $item, $depth, $args ) {
		if ( $args->theme_location !== self::$social_menu['theme_location'] ) {
			return $item_output;
		}

		foreach ( self::$icons as $attr => $value ) {
			if ( strpos( $item_output, $attr ) !== false ) {
				$svg_icon    = Helper::get_svg_icon( [ 'icon' => esc_attr( $value ) ] );
				$search      = $args->link_after;
				$replace     = "</span>$svg_icon";
				$item_output = str_replace( $search, $replace, $item_output );
			}
		}

		return $item_output;
	}

	public static function the_menu() {
		return self::get_menu( [ 'echo' => true ] );
	}

	public static function get_menu( $args = [] ) {
		$defaults = [
			'theme_location' => self::$social_menu['theme_location'],
			'menu_class'     => self::$social_menu['menu_class'],
			'container'      => '',
			'echo'           => false,
			'depth'          => 1,
			'link_before'    => '<span class="screen-reader-text">',
			'link_after'     => '</span>' . Helper::get_svg_icon( ( [ 'icon' => 'email' ] ) ),
			'fallback_cb'    => function() {
				esc_html_e( 'Please create social menu first', 'vnh' );
			},
		];

		$args = wp_parse_args( $args, $defaults );

		return wp_nav_menu( $args );
	}
}
