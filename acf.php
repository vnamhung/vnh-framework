<?php

namespace VNH\Framework;

class ACF {
	public function __construct() {
		add_action( 'acf/init', [ $this, 'acf_init' ] );
	}

	public function acf_init() {
		// General config
		acf_update_setting( 'path', Base::$cached['acf']['path'] );
		acf_update_setting( 'dir', Base::$cached['acf']['dir'] );
		acf_update_setting( 'google_api_key', Base::$theme['google_api_key'] );
		acf_update_setting( 'show_updates', false );
		acf_update_setting( 'show_admin', false );

		// Speed up ACF backend loading time
		add_filter( 'acf/settings/remove_wp_meta_box', '__return_true' );

		// Config translate
		acf_update_setting( 'l10n_textdomain', Base::$cached['text_domain'] );
		acf_update_setting( 'l10n_field', [ 'title', 'label', 'instructions', 'button_label' ] );

		if ( is_dev() ) {
			acf_update_setting( 'show_admin', true );

			// Config local JSON
			add_filter( 'acf/settings/load_json', [ $this, 'my_acf_json_load_point' ] );
			add_filter( 'acf/settings/save_json', [ $this, 'my_acf_json_save_point' ] );
		}
	}

	public function my_acf_json_save_point( $path ) {
		// update path
		$path = Base::$cached['child_directory'] . 'acf-json';

		return $path;
	}

	public function my_acf_json_load_point( $paths ) {
		// remove original path (optional)
		unset( $paths[0] );

		// append path
		$paths[] = Base::$cached['child_directory'] . 'acf-json';

		return $paths;
	}
}
