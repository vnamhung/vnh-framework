<?php
/**
 * Instagram Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

class Instagram extends Widget {
	public $display;

	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Instagram', 'vnh' ),
				'classname'   => 'widget-instagram',
				'description' => esc_html__( 'Displays your latest instagram photos.', 'vnh' ),
				'fields'      => [
					'title'    => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Latest Instagram Photos', 'vnh' ),
					],
					'username' => [
						'title'   => __( '@username or #tag:', 'vnh' ),
						'type'    => 'text',
						'default' => 'unsplash',
					],
					'number'   => [
						'title'   => __( 'Number of photos:', 'vnh' ),
						'type'    => 'number',
						'options' => [
							'min' => 1,
							'max' => 12,
						],
						'default' => 6,
					],
					'size'     => [
						'title'   => __( 'Photo size:', 'vnh' ),
						'type'    => 'dropdown',
						'options' => [
							'thumbnail' => esc_html__( 'Thumbnail', 'vnh' ),
							'small'     => esc_html__( 'Small', 'vnh' ),
							'large'     => esc_html__( 'Large', 'vnh' ),
							'original'  => esc_html__( 'Original', 'vnh' ),
						],
						'default' => 'large',
					],
				],
				'display'     => [
					'before'  => '<ul class="instagram__photos" data-size="%s">',
					'after'   => '</ul>',
					'content' => '<li class="instagram__photo"><a href="%1$s" rel="nofollow" target="_blank" ><img src="%2$s" alt="%3$s" /></a></li>',
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		self::before_widget_content( $args, $instance );

		if ( empty( $instance['username'] ) || empty( $instance['number'] ) || empty( $instance['size'] ) ) {
			return new \WP_Error( 'setting_error', esc_html__( 'Please fill all widget settings!', 'vnh' ) );
		}

		do_shortcode(
			sprintf(
				'[instagram username = %s number = %s size = %s transient_name = %s before=\'%s\' after=\'%s\' content=\'%s\' ]',
				$instance['username'],
				$instance['number'],
				$instance['size'],
				$this->id,
				$this->display['before'],
				$this->display['after'],
				$this->display['content']
			)
		);

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = sanitize_text_field( $new_instance['title'] );
		$instance['username'] = sanitize_text_field( $new_instance['username'] );
		$instance['number']   = ( (int) $new_instance['number'] !== 0 ) ? (int) $new_instance['number'] : null;
		$instance['size']     = ( ( 'thumbnail' === $new_instance['size'] || 'large' === $new_instance['size'] || 'small' === $new_instance['size'] || 'original' === $new_instance['size'] ) ? $new_instance['size'] : 'large' );

		if (
			$old_instance['username'] !== $new_instance['username'] ||
			$old_instance['number'] !== $new_instance['number'] ||
			$old_instance['size'] !== $new_instance['size']
		) {
			delete_transient( $this->id );
		}

		return $instance;
	}
}
