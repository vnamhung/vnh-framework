<?php

namespace VNH\Framework\Improvement;

class Purge_Transients {
	public $older_than = '1 day';

	public function __construct() {
		register_activation_hook( __FILE__, [ $this, 'purge_transients_activation' ] );
		register_deactivation_hook( __FILE__, [ $this, 'purge_transients_deactivation' ] );
		add_action( 'purge_transients_cron', [ $this, 'purge_transients' ] );
	}

	public function purge_transients( $safe_mode = true ) {
		global $wpdb;

		$older_than_time = strtotime( '-' . $this->older_than );

		if ( $older_than_time > time() || $older_than_time < 1 ) {
			return false;
		}

		$transients = $wpdb->get_col(
			$wpdb->prepare( "
					SELECT REPLACE(option_name, '_transient_timeout_', '') AS transient_name 
					FROM {$wpdb->options} 
					WHERE option_name LIKE '\_transient\_timeout\__%%'
					AND option_value < %s
			", $older_than_time )
		);

		if ( empty( $safe_mode ) ) {
			foreach ( $transients as $transient ) {
				delete_transient( $transient );
			}
		}

		return $transients;
	}

	public function purge_transients_activation() {
		if ( ! wp_next_scheduled( 'purge_transients_cron' ) ) {
			wp_schedule_event( time(), 'daily', 'purge_transients_cron' );
		}
	}

	public function my_plugin_deactivation() {
		if ( wp_next_scheduled( 'purge_transients_cron' ) ) {
			wp_clear_scheduled_hook( 'purge_transients_cron' );
		}
	}

}
