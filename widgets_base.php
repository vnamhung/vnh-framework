<?php

namespace VNH\Framework;

use VNH\Framework\Widgets\Mailchimp\Mailchimp_Init;

abstract class Widgets_Base {
	public $register_widget_areas;
	public $register_widgets;
	public $defaults = [
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget__title">',
		'after_title'   => '</h3>',
	];

	public function __construct() {
		$this->defaults              = apply_filters( 'vnh/f/widgets/defaults', $this->defaults );
		$this->register_widgets      = apply_filters( 'vnh/f/widgets/register/widgets', $this->register_widgets );
		$this->register_widget_areas = apply_filters( 'vnh/f/widgets/register/widget_areas', $this->register_widget_areas );

		add_action( 'widgets_init', [ $this, 'register_widget_areas' ] );
		add_action( 'widgets_init', [ $this, 'register_widgets' ] );

		if ( ! empty( $this->register_widgets['VNH\Framework\Widgets\Mailchimp'] ) ) {
			new Mailchimp_Init();
		}
	}

	public function register_widgets() {
		foreach ( $this->register_widgets as $widget => $status ) :
			if ( $status === true ) {
				register_widget( $widget );
			}
		endforeach;
	}

	public function register_widget_areas() {
		foreach ( $this->register_widget_areas as $widget_area ) {
			$this->register_widget_area( $widget_area );
		}
	}

	protected function register_widget_area( $args ) {
		$args = wp_parse_args( $args, $this->defaults );

		return register_sidebar( $args );
	}
}
