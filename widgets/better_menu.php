<?php
/**
 * Better Menu Widget
 *
 * @version 1.0.0
 */

namespace VNH\Framework\Widgets;

class Better_Menu extends Widget {
	public function __construct() {
		parent::__construct(
			[
				'base_id'     => self::create_widget_id( __CLASS__ ),
				'name'        => esc_html__( 'Better Menu', 'vnh' ),
				'classname'   => 'widget-better-menu',
				'description' => esc_html__( 'Display custom menu.', 'vnh' ),
				'fields'      => [
					'title'  => [
						'title'   => __( 'Title:', 'vnh' ),
						'type'    => 'text',
						'default' => esc_html__( 'Better Menu', 'vnh' ),
					],
					'nav_menu'     => [
						'title'   => __( 'Select Menu:', 'vnh' ),
						'type'    => 'dropdown_all_menus',
						'default' => '',
					],
				],
			]
		);
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		$nav_menu = wp_get_nav_menu_object( $instance['nav_menu'] ); // Get menu

		if ( ! $nav_menu ) {
			return;
		}

		self::before_widget_content( $args, $instance );

		wp_nav_menu( [
			'container'  => '',
			'menu_class' => 'better-menu',
			'menu'       => $nav_menu,
		] );

		self::after_widget_content( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = sanitize_text_field( $new_instance['title'] );
		$instance['nav_menu'] = $new_instance['nav_menu'];

		return $instance;
	}
}
