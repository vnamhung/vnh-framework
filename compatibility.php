<?php

namespace VNH\Framework;

class Compatibility {
	public function __construct() {
		if ( version_compare( $GLOBALS['wp_version'], Base::REQUIRE_VERSION, '<' ) ) :
			add_action( 'after_switch_theme', [ $this, 'switch_theme' ] );
			add_action( 'load-customize.php', [ $this, 'customize' ] );
			add_action( 'template_redirect', [ $this, 'preview' ] );
		endif;
	}

	public function switch_theme() {
		switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

		unset( $_GET['activated'] );

		add_action( 'admin_notices', [ $this, 'upgrade_notice' ] );
	}

	public function upgrade_notice() {
		/* translators: %1$s: Theme name. */
		/* translators: %2$s: Min WordPress version. */
		/* translators: %3$s: Current WordPress version. */
		$message = sprintf( __( '%1$s requires at least WordPress version %2$s. You are running version %3$s. Please upgrade and try again.', 'vnh' ), Base::$cached['theme_name'], Base::REQUIRE_VERSION, $GLOBALS['wp_version'] );
		printf( '<div class="error"><p>%s</p></div>', esc_html( $message ) );
	}

	public function customize() {
		/* translators: %1$s: Theme name. */
		/* translators: %2$s: Min WordPress version. */
		/* translators: %3$s: Current WordPress version. */
		$message = sprintf( __( '%1$s requires at least WordPress version %2$s. You are running version %3$s. Please upgrade and try again.', 'vnh' ), Base::$cached['theme_name'], Base::REQUIRE_VERSION, $GLOBALS['wp_version'] );
		wp_die( esc_html( $message ), '', [ 'back_link' => true ] );
	}

	public function preview() {
		if ( isset( $_GET['preview'] ) ) {
			/* translators: %1$s: Theme name. */
			/* translators: %2$s: Min WordPress version. */
			/* translators: %3$s: Current WordPress version. */
			$message = sprintf( __( '%1$s requires at least WordPress version %2$s. You are running version %3$s. Please upgrade and try again.', 'vnh' ), Base::$cached['theme_name'], Base::REQUIRE_VERSION, $GLOBALS['wp_version'] );
			wp_die( esc_html( $message ) );
		}
	}
}
