<?php

namespace VNH\Framework;

class Menus {
	public static $menus;

	public function __construct() {
		$defaults    = [
			'primary' => esc_html__( 'Primary', 'vnh' ),
		];
		self::$menus = apply_filters( 'vnh/f/menus/register', Base::$menus );
		self::$menus = wp_parse_args( (array) self::$menus, $defaults );

		add_action( 'after_setup_theme', [ $this, 'register_nav_menus' ] );
		add_filter( 'wp_page_menu_args', [ $this, 'page_menu_args' ] );
		add_filter( 'nav_menu_item_title', [ $this, 'dropdown_icon_to_menu_link' ], 10, 4 );
	}

	public function register_nav_menus() {
		register_nav_menus( self::$menus );
	}

	public function page_menu_args( $args ) {
		$args['show_home'] = true;

		return $args;
	}

	public function dropdown_icon_to_menu_link( $title, $item, $args, $depth ) {
		foreach ( $item->classes as $value ) {
			if ( $value === 'menu-item-has-children' || $value === 'page_item_has_children' ) {
				$title = $title . Helper::get_svg_icon( [ 'icon' => 'angle-down' ] );
			}
		}

		return $title;
	}
}
