<?php

namespace VNH\Framework;

class Breadcrumb {
	protected $args;

	public function __construct() {
		$defaults             = [
			'home'                    => esc_html__( 'Home', 'vnh' ),
			'sep'                     => esc_html__( '<span aria-label="breadcrumb separator">/</span> ', 'vnh' ),
			'list_sep'                => ', ',
			'prefix'                  => '<div class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">',
			'suffix'                  => '</div>',
			'heirarchial_attachments' => true,
			'heirarchial_categories'  => true,
			'labels'                  => [
				'prefix'    => esc_html__( 'You are here: ', 'vnh' ),
				'author'    => esc_html__( 'Archives for ', 'vnh' ),
				'category'  => esc_html__( 'Archives for ', 'vnh' ),
				'tag'       => esc_html__( 'Archives for ', 'vnh' ),
				'date'      => esc_html__( 'Archives for ', 'vnh' ),
				'search'    => esc_html__( 'Search for ', 'vnh' ),
				'tax'       => esc_html__( 'Archives for ', 'vnh' ),
				'post_type' => esc_html__( 'Archives for ', 'vnh' ),
				'404'       => esc_html__( 'Not found: ', 'vnh' ),
			],
		];
		$this->args           = apply_filters( 'vnh/f/breadcrumb/args', Base::$breadcrumb_args );
		$this->args           = wp_parse_args( (array) $this->args, $defaults );
		$this->args['labels'] = wp_parse_args( (array) $this->args['labels'], $defaults['labels'] );

		add_action( 'vnh/h/breadcrumb', [ $this, 'output' ] );
	}

	public function __toString() {
		return (string) $this->get_output();
	}

	public function output() {
		echo $this->get_output(); //WPCS XSS ok
	}

	public function get_output() {
		return $this->args['prefix'] . $this->args['labels']['prefix'] . $this->build_crumbs() . $this->args['suffix'];
	}

	protected function build_crumbs() {
		$crumbs[] = $this->get_home_crumb();

		if ( is_home() ) {
			$crumbs[] = $this->get_blog_crumb();
		} elseif ( is_search() ) {
			$crumbs[] = $this->get_search_crumb();
		} elseif ( is_404() ) {
			$crumbs[] = $this->get_404_crumb();
		} elseif ( is_page() ) {
			$crumbs[] = $this->get_page_crumb();
		} elseif ( is_archive() ) {
			$crumbs[] = $this->get_archive_crumb();
		} elseif ( is_singular() ) {
			$crumbs[] = $this->get_single_crumb();
		}

		$crumbs = apply_filters( 'vnh/f/breadcrumb/build', $crumbs, $this->args );

		return implode( $this->args['sep'], array_filter( array_unique( $crumbs ) ) );
	}

	protected function get_archive_crumb( $crumb = '' ) {
		if ( is_category() ) {
			$crumb = $this->get_category_crumb();
		} elseif ( is_tag() ) {
			$crumb = $this->get_tag_crumb();
		} elseif ( is_tax() ) {
			$crumb = $this->get_tax_crumb();
		} elseif ( is_year() ) {
			$crumb = $this->get_year_crumb();
		} elseif ( is_month() ) {
			$crumb = $this->get_month_crumb();
		} elseif ( is_day() ) {
			$crumb = $this->get_day_crumb();
		} elseif ( is_author() ) {
			$crumb = $this->get_author_crumb();
		} elseif ( is_post_type_archive() ) {
			$crumb = $this->get_post_type_crumb();
		}

		return apply_filters( 'vnh/f/breadcrumb/archive', $crumb, $this->args );
	}

	protected function get_single_crumb() {
		if ( is_attachment() ) {
			$crumb = $this->get_attachment_crumb();
		} elseif ( is_singular( 'post' ) ) {
			$crumb = $this->get_post_crumb();
		} else {
			$crumb = $this->get_cpt_crumb();
		}

		return apply_filters( 'vnh/f/breadcrumb/single', $crumb, $this->args );
	}

	protected function get_home_crumb() {
		$url   = $this->page_shown_on_front() ? get_permalink( get_option( 'page_on_front' ) ) : trailingslashit( home_url() );
		$crumb = ( is_home() && is_front_page() ) ? $this->args['home'] : $this->get_breadcrumb_link( $url, $this->args['home'] );

		return apply_filters( 'vnh/f/breadcrumb/home', $crumb, $this->args );
	}

	protected function get_blog_crumb() {
		$crumb = $this->get_home_crumb();
		if ( $this->page_shown_on_front() ) {
			$crumb = get_the_title( get_option( 'page_for_posts' ) );
		}

		return apply_filters( 'vnh/f/breadcrumb/blog', $crumb, $this->args );
	}

	protected function get_search_crumb() {
		$crumb = sprintf( '%s "%s"', $this->args['labels']['search'], esc_html( get_search_query() ) );

		return apply_filters( 'vnh/f/breadcrumb/search', $crumb, $this->args );
	}

	protected function get_404_crumb() {
		$crumb = $this->args['labels']['404'];

		return apply_filters( 'vnh/f/breadcrumb/404', $crumb, $this->args );
	}

	protected function get_page_crumb() {
		global $wp_query;

		if ( $this->page_shown_on_front() && is_front_page() ) {
			// Don't do anything - we're on the front page and we've already dealt with that elsewhere.
			$crumb = $this->get_home_crumb();
		} else {
			$post = $wp_query->get_queried_object();

			// If this is a top level Page, it's simple to output the breadcrumb.
			if ( ! $post->post_parent ) {
				$crumb = get_the_title();
			} else {
				if ( isset( $post->ancestors ) ) {
					if ( is_array( $post->ancestors ) ) {
						$ancestors = array_values( $post->ancestors );
					} else {
						$ancestors = [ $post->ancestors ];
					}
				} else {
					$ancestors = [ $post->post_parent ];
				}

				$crumbs = [];
				foreach ( $ancestors as $ancestor ) {
					array_unshift( $crumbs, $this->get_breadcrumb_link( get_permalink( $ancestor ), get_the_title( $ancestor ) ) );
				}

				// Add the current page title.
				$crumbs[] = get_the_title( $post->ID );

				$crumb = implode( $this->args['sep'], $crumbs );
			}
		}

		return apply_filters( 'vnh/f/breadcrumb/page', $crumb, $this->args );
	}

	protected function get_attachment_crumb( $crumb = '' ) {
		if ( $this->args['heirarchial_attachments'] ) {
			// If showing attachment parent.
			$post              = get_post();
			$attachment_parent = get_post( $post->post_parent );
			$url               = get_permalink( $post->post_parent );
			$content           = $attachment_parent->post_title;
			$crumb             = $this->get_breadcrumb_link( $url, $content, $this->args['sep'] );
		}
		$crumb .= single_post_title( '', false );

		return apply_filters( 'vnh/f/breadcrumb/attachment', $crumb, $this->args );
	}

	protected function get_post_crumb( $crumbs = [], $cat_crumb = '' ) {
		$categories = get_the_category();

		if ( count( $categories ) === 1 ) {
			// If in single category, show it, and any parent categories.
			$cat_crumb = $this->get_term_parents( $categories[0]->cat_ID, 'category', true ) . $this->args['sep'];
		}
		if ( count( $categories ) > 1 ) {
			if ( ! $this->args['heirarchial_categories'] ) {
				// Don't show parent categories (unless the post happen to be explicitly in them).
				foreach ( $categories as $category ) {
					$crumbs[] = $this->get_breadcrumb_link( get_category_link( $category->term_id ), $category->name );
				}

				$cat_crumb = implode( $this->args['list_sep'], $crumbs ) . $this->args['sep'];
			} else {
				// Show parent categories - see if one is marked as primary and try to use that.
				$primary_category_id = get_post_meta( get_the_ID(), '_category_permalink', true ); // Support for sCategory Permalink plugin.

				if ( ! $primary_category_id && function_exists( 'yoast_get_primary_term_id' ) ) {
					// Support for Yoast SEO plugin, even if the Yoast Breadcrumb feature is not enabled.
					$primary_category_id = yoast_get_primary_term_id();
				}

				if ( $primary_category_id ) {
					$cat_crumb = $this->get_term_parents( $primary_category_id, 'category', true ) . $this->args['sep'];
				} else {
					$cat_crumb = $this->get_term_parents( $categories[0]->cat_ID, 'category', true ) . $this->args['sep'];
				}
			}
		}

		$crumb = $cat_crumb . single_post_title( '', false );

		return apply_filters( 'vnh/f/breadcrumb/post', $crumb, $this->args, $cat_crumb );
	}

	protected function get_cpt_crumb() {
		$post_type        = get_query_var( 'post_type' );
		$post_type_object = get_post_type_object( $post_type );

		$cpt_archive_link = get_post_type_archive_link( $post_type );
		if ( $cpt_archive_link ) {
			$crumb = $this->get_breadcrumb_link( $cpt_archive_link, $post_type_object->labels->name );
		} else {
			$crumb = $post_type_object->labels->name;
		}

		$crumb .= $this->args['sep'] . single_post_title( '', false );

		return apply_filters( 'vnh/f/breadcrumb/cpt', $crumb, $this->args );
	}

	protected function get_category_crumb() {
		$crumb = $this->args['labels']['category'] . $this->get_term_parents( get_query_var( 'cat' ), 'category' );

		return apply_filters( 'vnh/f/breadcrumb/category', $crumb, $this->args );
	}

	protected function get_tag_crumb() {
		$crumb = $this->args['labels']['tag'] . single_term_title( '', false );

		return apply_filters( 'vnh/f/breadcrumb/tag', $crumb, $this->args );
	}

	protected function get_tax_crumb() {
		global $wp_query;

		$term  = $wp_query->get_queried_object();
		$crumb = $this->args['labels']['tax'] . $this->get_term_parents( $term->term_id, $term->taxonomy );

		return apply_filters( 'vnh/f/breadcrumb/tax', $crumb, $this->args );
	}

	protected function get_year_crumb() {
		$year = get_query_var( 'm' ) ? get_query_var( 'm' ) : get_query_var( 'year' );

		$crumb = $this->args['labels']['date'] . $year;

		return apply_filters( 'vnh/f/breadcrumb/year', $crumb, $this->args );
	}

	protected function get_month_crumb() {
		$year = get_query_var( 'm' ) ? mb_substr( get_query_var( 'm' ), 0, 4 ) : get_query_var( 'year' );

		$crumb  = $this->get_breadcrumb_link( get_year_link( $year ), $year, $this->args['sep'] );
		$crumb .= $this->args['labels']['date'] . single_month_title( ' ', false );

		return apply_filters( 'vnh/f/breadcrumb/month', $crumb, $this->args );
	}

	protected function get_day_crumb() {
		global $wp_locale;

		$year  = get_query_var( 'm' ) ? mb_substr( get_query_var( 'm' ), 0, 4 ) : get_query_var( 'year' );
		$month = get_query_var( 'm' ) ? mb_substr( get_query_var( 'm' ), 4, 2 ) : get_query_var( 'monthnum' );
		$day   = get_query_var( 'm' ) ? mb_substr( get_query_var( 'm' ), 6, 2 ) : get_query_var( 'day' );

		$crumb  = $this->get_breadcrumb_link( get_year_link( $year ), $year, $this->args['sep'] );
		$crumb .= $this->get_breadcrumb_link( get_month_link( $year, $month ), $wp_locale->get_month( $month ), $this->args['sep'] );
		$crumb .= $this->args['labels']['date'] . $day . date( 'S', mktime( 0, 0, 0, 1, $day ) );

		return apply_filters( 'vnh/f/breadcrumb/day', $crumb, $this->args );
	}

	protected function get_author_crumb() {
		global $wp_query;

		$crumb = $this->args['labels']['author'] . esc_html( $wp_query->queried_object->display_name );

		return apply_filters( 'vnh/f/breadcrumb/author', $crumb, $this->args );
	}

	protected function get_post_type_crumb() {
		$crumb = $this->args['labels']['post_type'] . esc_html( post_type_archive_title( '', false ) );

		return apply_filters( 'vnh/f/breadcrumb/post_type', $crumb, $this->args );
	}

	protected function get_term_parents( $parent_id, $taxonomy, $link = false, array $visited = [] ) {
		$parent = get_term( (int) $parent_id, $taxonomy );
		$url    = get_term_link( get_term( $parent->term_id, $taxonomy ), $taxonomy );

		if ( is_wp_error( $parent ) ) {
			return [];
		}

		if ( $parent->parent && ( $parent->parent !== $parent->term_id ) && ! in_array( $parent->parent, $visited, true ) ) {
			$visited[] = $parent->parent;
			$chain[]   = $this->get_term_parents( $parent->parent, $taxonomy, true, $visited );
		}

		if ( $link && ! is_wp_error( $url ) ) {
			$chain[] = $this->get_breadcrumb_link( $url, $parent->name );
		} else {
			$chain[] = $parent->name;
		}

		return implode( $this->args['sep'], $chain );
	}

	private function get_breadcrumb_link( $url, $content, $sep = '' ) {
		$a_tag  = sprintf( '<a href="%s" itemprop="item"><span itemprop="name">%s</span></a>', esc_attr( $url ), $content );
		$before = '<span class="breadcrumb__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';
		$after  = '</span>';

		$link = $before . $a_tag . $after;

		if ( ! empty( $sep ) ) {
			$link .= $sep;
		}

		return $link;
	}

	protected function page_shown_on_front() {
		return get_option( 'show_on_front' ) === 'page';
	}
}
