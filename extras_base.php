<?php

namespace VNH\Framework;

abstract class Extras_Base {
	public static $extras;

	public function __construct() {
		$defaults     = [
			'cat_count_span'       => true,
			'pingback_header'      => true,
			'theme_meta_generator' => true,
			'template_redirect'    => true,
		];
		self::$extras = apply_filters( 'vnh/f/extras', Base::$extras );
		self::$extras = wp_parse_args( (array) self::$extras, $defaults );

		add_filter( 'get_archives_link', [ $this, 'cat_count_span' ] );
		add_filter( 'wp_list_categories', [ $this, 'cat_count_span' ] );

		add_action( 'wp_head', [ $this, 'pingback_header' ] );

		add_action( 'wp_head', [ $this, 'theme_meta_generator' ], 1 );

		add_action( 'template_redirect', [ $this, 'template_redirect' ] );

		add_filter( 'wp_resource_hints', [ $this, 'resource_hints' ], 10, 2 );
		add_action( 'script_loader_tag', [ $this, 'add_async' ], 10, 2 );
	}

	public function cat_count_span( $links ) {
		preg_match_all( '#\((.*?)\)#', $links, $matches );
		if ( ! empty( $matches ) && ! empty( self::$extras['cat_count_span'] ) ) {
			$i = 0;
			foreach ( $matches[0] as $value ) {
				$links = str_replace( "</a> $value", "</a><span>$value</span>", $links );
				$i++;
			}
		}

		return $links;
	}

	public function pingback_header() {

		if ( empty( self::$extras['pingback_header'] ) ) {
			return;
		}

		if ( is_singular() && pings_open() ) {
			printf( '<link rel="pingback" href="%s">' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
		}
	}

	public function theme_meta_generator() {
		if ( empty( self::$extras['theme_meta_generator'] ) ) {
			return;
		}

		$child_theme_activate = is_child_theme() ? 'and child theme is activated.' : '';

		printf( "\r\n" . '<meta name="generator" content="This site use %s Theme %s by %s %s" />' . "\r\n", esc_attr( Base::$cached['theme_name'] ), esc_attr( Base::$cached['theme_version'] ), esc_attr( Base::$cached['theme_author'] ), esc_attr( $child_theme_activate ) );
	}

	public function template_redirect() {
		if ( empty( self::$extras['template_redirect'] ) ) {
			return;
		}

		global $wp_query, $post;

		if ( is_attachment() ) {
			$post_parent = $post->post_parent;

			if ( $post_parent ) {
				wp_safe_redirect( get_permalink( $post->post_parent ), 301 );
				exit;
			}

			$wp_query->set_404();

			return;
		}

		if ( is_author() || is_date() ) {
			$wp_query->set_404();
		}
	}

	public function resource_hints( $urls, $relation_type ) {
		if ( wp_style_is( Base::$theme['google_fonts_handle'], 'queue' ) && $relation_type === 'preconnect' ) {
			$urls[] = [
				'href' => 'https://fonts.gstatic.com',
				'crossorigin',
			];
		}

		return $urls;
	}

	public function add_async( $tag, $handle ) {
		return ( $handle === Base::$theme['main_js_handle'] ) ? preg_replace( '/(><\/[a-zA-Z][^0-9](.*)>)$/', ' async $1 ', $tag ) : $tag;
	}
}
