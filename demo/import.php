<?php

namespace VNH\Framework\Demo;

use VNH\Framework\Base;
use VNH\Framework\Helper;

class Import {
	public static $import_files, $default_import_files, $ocdi_settings, $default_ocdi_settings;

	public function __construct() {
		self::$default_import_files = [
			[
				'import_file_name'           => 'Demo Import',
				'categories'                 => [ __( 'Business', 'vnh' ), __( 'Portfolio', 'vnh' ) ],
				'import_file_url'            => get_parent_theme_file_uri( Base::SAMPLE_DIR . 'content.xml' ),
				'import_widget_file_url'     => get_parent_theme_file_uri( Base::SAMPLE_DIR . 'widgets.json' ),
				'import_customizer_file_url' => get_parent_theme_file_uri( Base::SAMPLE_DIR . 'customizer.dat' ),
				'import_preview_image_url'   => get_parent_theme_file_uri( 'screenshot.png' ),
				'preview_url'                => Base::$info['theme_uri'],
			],
		];
		self::$import_files         = apply_filters( 'vnh/f/import/files', Base::$import_files );
		self::$import_files         = wp_parse_args( (array) self::$import_files, self::$default_import_files );

		self::$default_ocdi_settings = [
			'parent_slug' => 'themes.php',
			'page_title'  => esc_html__( 'One Click Demo Import', 'vnh' ),
			'menu_title'  => esc_html__( 'Import Demo Data', 'vnh' ),
			'capability'  => 'import',
			'menu_slug'   => Base::PREFIX . '-demo-import',
		];
		self::$ocdi_settings         = apply_filters( 'vnh/f/import/settings', Base::$ocdi_settings );
		self::$ocdi_settings         = wp_parse_args( (array) self::$ocdi_settings, self::$default_ocdi_settings );

		add_filter( 'pt-ocdi/import_files', [ $this, 'import_files' ] );
		add_action( 'pt-ocdi/after_import', [ $this, 'import_menus' ] );
		add_action( 'pt-ocdi/after_import', [ $this, 'import_homepage_settings' ] );

		add_filter( 'pt-ocdi/plugin_page_setup', [ $this, 'plugin_page_setup' ] );

		add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );
		add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );
	}

	public function import_files() {
		return self::$import_files;
	}

	public function plugin_page_setup() {
		return self::$ocdi_settings;
	}

	public function import_homepage_settings() {
		$pages = $this->get_data( 'homepage' );

		if ( is_array( $pages ) ) {
			if ( ! empty( $pages['show_on_front'] ) ) {
				update_option( 'show_on_front', $pages['show_on_front'] );
			}

			if ( ! empty( $pages['page_on_front'] ) ) {
				$page = get_page_by_title( $pages['page_on_front'] );

				update_option( 'page_on_front', $page->ID );
			}

			if ( ! empty( $pages['page_for_posts'] ) ) {
				$page = get_page_by_title( $pages['page_for_posts'] );

				update_option( 'page_for_posts', $page->ID );
			}

			// Move Hello World post to trash
			wp_trash_post( 1 );

			// Move Sample Page to trash
			wp_trash_post( 2 );
		}
	}

	public function import_menus() {
		$menu_data  = $this->get_data( 'menus' );
		$menu_array = [];

		if ( ! empty( $menu_data ) ) {
			foreach ( $menu_data as $registered_menu => $menu_slug ) {
				$menu_array[ $registered_menu ] = get_term_by( 'slug', $menu_slug, 'nav_menu' )->term_id;
			}

			set_theme_mod( 'nav_menu_locations', array_map( 'absint', $menu_array ) );
		}
	}

	private function get_data( $type ) {
		$file = Base::$cached['parent_directory'] . Base::SAMPLE_DIR . $type . '.json';

		if ( ! file_exists( $file ) ) {
			return '';
		}

		return json_decode( Helper::get_file_content( $file ), true );
	}
}
