<?php

namespace VNH\Framework\Widgets\Mailchimp;

use VNH\Framework\Base;
use VNH\Framework\Cached;

class Mailchimp_Init {
	public function __construct() {
		// Enqueue plugin admin assets.
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_admin_scripts' ] );
	}

	public function enqueue_admin_scripts() {
		// Enqueue admin JS.
		wp_enqueue_script( 'mailchimp-admin-js', Base::$cached['widgets']['mailchimp_admin_js_url'], [ 'jquery' ], null, true );

		// Provide the global variable to the 'mailchimp-admin-js'.
		wp_localize_script( 'mailchimp-admin-js', 'PTMCWAdminVars', [
			'ajax_url'   => admin_url( 'admin-ajax.php' ),
			'ajax_nonce' => wp_create_nonce( 'pt-mcw-ajax-verification' ),
			'text'       => [
				'ajax_error'        => esc_html__( 'An error occurred while retrieving data via the AJAX request!', 'vnh' ),
				'no_api_key'        => esc_html__( 'Please input the MailChimp API key!', 'vnh' ),
				'incorrect_api_key' => esc_html__( 'This MailChimp API key is not formatted correctly, please copy the whole API key from the MailChimp dashboard!', 'vnh' ),
			],
		] );
	}
}
