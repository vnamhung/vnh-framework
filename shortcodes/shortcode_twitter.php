<?php

namespace VNH\Framework\Shortcodes;

use VNH\Framework\Base;
use VNH\Framework\Helper;
use Abraham\TwitterOAuth\TwitterOAuth;

class Shortcode_Twitter {
	public static $defaults;

	public function __construct() {
		self::$defaults = [
			'transient_name'      => 'vnh_shortcode_twitter',
			'consumer_key'        => '',
			'consumer_secret'     => '',
			'access_token'        => '',
			'access_token_secret' => '',
			'username'            => '',
			'tweets_to_show'      => '1',
			'before'              => '<ul class="twitter__tweets">',
			'after'               => '</ul>',
			'content'             => '<li class="twitter__tweet"><span>%1$s</span><a class="twitter__permalink" href="%2$s" target="_blank" rel="nofollow"><time class="twitter__time" datetime="%3$s">%4$s</time></a></li>',
		];
		self::$defaults = apply_filters( 'vnh/f/shortcode/twitter/defaults', self::$defaults );

		add_shortcode( 'twitter', [ $this, 'create_shortcode' ] );
	}

	public function create_shortcode( $atts ) {
		$atts = shortcode_atts( self::$defaults, $atts );

		$html = $atts['before'];

		foreach ( $this->get_tweets( $atts ) as $tweet ) {
			$text      = $this->convert_links( $tweet->text );
			$user_name = $atts['username'];
			$status_id = $tweet->id_str;
			$link      = "https://twitter.com/$user_name/statuses/$status_id";
			$time      = strtotime( $tweet->created_at );

			if ( empty( $tweet->text ) ) {
				return;
			}

			$html .= sprintf( $atts['content'], $text, esc_url( $link ), esc_attr( date( 'c', $time ) ), esc_html( Helper::human_time_diff_maybe( $time, 7 ) ) );
		}

		$html .= $atts['after'];

		echo $html; // WPCS XSS ok
	}

	protected function get_tweets( $atts ) {
		$cached_tweets = get_transient( $atts['transient_name'] );

		// If the tweets is cached, use them & early exit.
		if ( ! empty( $cached_tweets ) ) {
			return $cached_tweets;
		}

		// If we got this far, the tweets is not cached. We'll need to get them, and add them to the cache.
		require_once Base::$cached['twitter_oauth_path'];

		$connection = new TwitterOAuth( $atts['consumer_key'], $atts['consumer_secret'], $atts['access_token'], $atts['access_token_secret'] );
		$tweets     = $connection->get( 'statuses/user_timeline', [
			'screen_name'     => $atts['username'],
			'count'           => $atts['tweets_to_show'],
			'exclude_replies' => true,
		] );

		// Check tweets and die if not error
		if ( ! empty( $tweets->errors ) ) {
			return new \WP_Error( 'error', $tweets->errors[0]->message );
		}

		set_transient( $atts['transient_name'], $tweets, apply_filters( 'vnh/f/shortcode/twitter/cache/expiration', Base::SHORT_EXPIRATION ) ); // Cache for 1 hour using transients.

		return $tweets;
	}

	protected function convert_links( $status, $target_blank = true ) {
		// the target
		$target = $target_blank ? ' target="_blank" ' : '';

		// convert link to url
		$status = preg_replace( '/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[A-Z0-9+&@#\/%=~_|]/i', '<a href="\0" rel="nofollow" target="_blank">\0</a>', $status );

		// convert @ to follow
		$status = preg_replace( '/(@([_a-z0-9\\-]+))/i', "<a href='http://twitter.com/$2' title='Follow $2' rel='nofollow' $target >$1</a>", $status );

		// convert # to search
		$status = preg_replace( '/(#([_a-z0-9\\-]+))/i', "<a href='https://twitter.com/search?q=$2' title='Search $1' rel='nofollow' $target >$1</a>", $status );

		// return the status
		return $status;
	}
}
